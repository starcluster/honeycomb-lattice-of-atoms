#include <iostream>
#include <complex>
#include <Eigen/Dense>
#include <sstream>

#include "utils.h"
#include "Params.h"

using namespace std;
using namespace Eigen;

double computeCorr(Params p, int i_batch);


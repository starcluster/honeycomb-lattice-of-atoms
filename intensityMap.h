#include <fstream>
#include <complex>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

void intensityMap(MatrixXcd Q, double *freqs, Vector2d *hexlattice, int ivector, int NN);
void intensityMap2(MatrixXcd Q, double *freqs, Vector2d *hexlattice, int ivector, int N);

#include "utils.h"

double omegaPractical(double *freqs, double omega, int *ivector, int N_atoms){
  double gap_to_omega = 300.;
  double omega_practical = 0;
  for(int i = 0; i < 2*N_atoms; i++){
    if (abs(freqs[i] - omega) < gap_to_omega){
      gap_to_omega = abs(freqs[i] - omega);
      omega_practical = freqs[i];
      (*ivector) = i;
    }
  }
  return omega_practical;
}

void fileError(){
  cout << "Can't open file: ABORT !" <<endl;
  abort();
}

// 3 versions de general procedure: intolérable à améliorer en modifiant les boucles ?

MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY){
  int NN = p.N*p.N;
  MatrixXcd G(4*NN, 4*NN);
  Vector2d  *hexlattice;
  hexlattice = new Vector2d[2*NN];
  generateG2D(&p, hexlattice, &G);
  ComplexEigenSolver<MatrixXcd> eigensolver(G);
  if (eigensolver.info() != Success) abort();
  MatrixXcd Q(4*NN, 4*NN);
  Q = eigensolver.eigenvectors();



  
  complex<double> eigenvalues[4*NN];
  for (int i = 0; i < 4*NN; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
  eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
  eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
  sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
  computeUXUY(UX, UY, hexlattice, NN);
  computeVXVY(UX, UY, VX, VY, &Q, NN);
  return Q;
}

MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, double random){
  int NN = p.N*p.N;
  MatrixXcd G(4*NN, 4*NN);
  Vector2d  *hexlattice;
  hexlattice = new Vector2d[2*NN];
  generateG2D(&p, hexlattice, &G);
  ComplexEigenSolver<MatrixXcd> eigensolver(G);
  if (eigensolver.info() != Success) abort();
  MatrixXcd Q(4*NN, 4*NN);
  Q = eigensolver.eigenvectors();
  complex<double> eigenvalues[4*NN];
  for (int i = 0; i < 4*NN; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
  eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
  eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
  sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
  computeUXUY(UX, UY, hexlattice, p.N_atoms);
  computeVXVY(UX, UY, VX, VY, &Q, p.N_atoms);
  return Q;
}

MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, double delta_B, double delta_AB){
  int NN = p.N*p.N;
  MatrixXcd G(4*NN, 4*NN);
  Vector2d  *hexlattice;
  hexlattice = new Vector2d[2*NN];
  generateG2D(&p, hexlattice, &G);
  ComplexEigenSolver<MatrixXcd> eigensolver(G);
  if (eigensolver.info() != Success) abort();
  MatrixXcd Q(4*NN, 4*NN);
  Q = eigensolver.eigenvectors();
  complex<double> eigenvalues[4*NN];
  for (int i = 0; i < 4*NN; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
  eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
  eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
  sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
  computeUXUY(UX, UY, hexlattice, p.N_atoms);
  computeVXVY(UX, UY, VX, VY, &Q, p.N_atoms);
  return Q;
}


MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, Vector2d *hexlattice){
  int NN = p.N*p.N;
  MatrixXcd G(4*NN, 4*NN);
  
  double t0 = omp_get_wtime();
  double t0_cpu = clock();

  generateG2D(&p, hexlattice, &G);

  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  cout << "Temps reel (GENERATE g): " << t1-t0 << endl;
  cout << "Temps CPU (GENERATE g): " << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;

  double t2 = omp_get_wtime();
  double t2_cpu = clock();
  ComplexEigenSolver<MatrixXcd> eigensolver(G);
  double t3 = omp_get_wtime();
  double t3_cpu = clock();
  cout << "Temps reel (EIGENSOLVER): " << t3-t2 << endl;
  cout << "Temps CPU (EIGENSOLVER): " << (t3_cpu-t2_cpu)/CLOCKS_PER_SEC << endl;
  if (eigensolver.info() != Success) abort();
  MatrixXcd Q(4*NN, 4*NN);
  Q = eigensolver.eigenvectors();


  
  complex<double> eigenvalues[4*NN];
  for (int i = 0; i < 4*NN; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);





  
  eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);

  //tmp lines of code
  double border;
  ofstream f_freq2(p.output + "freq2.dat");
  f_freq2 << "f" <<endl;
  for(int ivector = 0; ivector < 4*NN; ivector++){
    border = 0;
    for(int i = 0; i < 2*NN; i++){
      if ((i < p.N) or (i > 2*NN-p.N-1) or (i%p.N == 0) or ((i+1)%p.N == 0) ){
	border += norm(Q.col(ivector)(2*i));
	border += norm(Q.col(ivector)(2*i+1));
      }
    }
    if (border < 0.7){
      f_freq2 << freqs[ivector] << endl;
    }
  }

  
  int cc = 0;
  for(int i = 0; i < 2*NN; i++){
    if ((i < p.N) or (i > 2*NN-p.N-1) or (i%p.N == 0) or ((i+1)%p.N == 0) ){
      cc++;
    } else {
      // f_freq2 << freqs[2*i] << endl;
      // f_freq2 << freqs[2*i+1] << endl;
    }
  }
  cout << "pourcentage de suppression " << ((double)cc)/(4*NN) <<endl;
  
  eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
  sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
  computeUXUY(UX, UY, hexlattice, p.N_atoms);
  computeVXVY(UX, UY, VX, VY, &Q, p.N_atoms);
  return Q;
}

void show_usage(std::string name){
    std::cerr << "Usage: " << name << " <option(s)>\n"
              << "Options:\n"
              << "\t-h,--help\t\t Show this help message\n"
	      << "\t-v,--verbose\t\t Verbose mode\n"
      	      << "\t-o,--output \t\t To specify a directory in ../data\n"
	      << "\t-t,--num_threads \t To specify the number of threads\n"
	      << "\n\n" 
              << "\t-N \t\t\t Number of atoms is 2*N*N\n"
	      << "\t-a \t\t\t Step of the network\n"
	      << "\t-r \t\t\t Randomize the network (0--100%)\n"
	      << "\t--delta_B \t\t Magnetic field.\n"
      	      << "\t--delta_AB \t\t Difference between sites A and B\n"
	      << "\t--omega \t\t Specify omega for bott index and intensity map\n"
	      << "\t--omega_r \t\t Bounding of the omega/r map and step, no intensity map is done\n"
      	      << "\t--bott_map \t\t Bounding of the bott_map and step, no intensity map is done\n"
	      << "\t--N_batch \t\t Number of map created, only if --omega_r or --bott_map activated\n"
      	      << "\t--corr \t\t\t Compute correlation for increasing disorder\n"

      
              << std::endl;
}

void parser(int argc, char **argv, Params *p){
    if (argc < 2) {
    cout << "No options specified. Default options are:\n"
	 << "N = 10 (200 atoms)\n"
	 << "a = 1\n"
	 << "delta_B = 12\n"
	 << "delta_AB = 0\n"
	 << "omega = 0\n"
	 << "r = 0\n"
	 << "N_batch = 0\n"
      	 << "num_threads = 2\n"
	 << endl;
        show_usage(argv[0]);
	ofstream f_params(p->output + "params.dat");
	if (f_params){f_params << p->N << endl << p->a <<endl << p->delta_B << endl << p->delta_AB <<endl << (p->random)/100. << endl << p->N_batch << endl << p->omega << endl;}
	else {fileError();}
	f_params.close();
        return;
    }
  const char s = ',';
  string arg_bott_map;
  string arg_omega_r;
      for (int i = 1; i < argc; ++i) {
      std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
	  show_usage(argv[0]);
	  exit(0);
        } else if (arg == "-N") {
 	  if (i + 1 < argc) {
	    p->N = stoi(argv[i+1]);
	    p->N_atoms = 2*p->N*p->N; //by default we work on a grid
	    i++;
            } else { 
	      std::cerr << "-N option requires one argument." << std::endl;
		  exit(1);
            }  
	} else if (arg == "-a") {
	  if (i + 1 < argc) { 
	    p->a = stod(argv[i+1]);
	    i++;
            } else {
	      std::cerr << "-a option requires one argument." << std::endl;
		  exit(1);
            }  
	} else if (arg == "--delta_B") {
	  if (i + 1 < argc) { 
	    p->delta_B = stod(argv[i+1]);
	    i++;
            } else {
	      std::cerr << "--delta_B option requires one argument." << std::endl;
		  exit(1);
            }  
	} else if (arg == "--delta_AB") {
	  if (i + 1 < argc) { 
	    p->delta_AB = stod(argv[i+1]);
	    i++;
            } else {
	      std::cerr << "--delta_AB option requires one argument." << std::endl;
		  exit(1);
            }  
	} else if (arg == "-r") {
	  if (i + 1 < argc) { 
	    p->random = stoi(argv[i+1]);
	    i++;
            } else {
	      std::cerr << "-r option requires one argument." << std::endl;
		  exit(1);
            }  
	} else if (arg == "--bott_map") {
	  if (i + 1 < argc) {
	    p->bott_map_bool = true;
	    arg_bott_map = argv[i+1];
	    i++;
            } else {
	      std::cerr << "--bott_map option requires one argument." << std::endl;
		  exit(1);
            }  
	} else if (arg == "--omega_r") {
	  if (i + 1 < argc) {
	    p->omega_r_bool = true;
	    arg_omega_r = argv[i+1];
	    i++;
            } else {
	      std::cerr << "--omega_r option requires one argument." << std::endl;
		  exit(1);
	  }
	}  else if (arg == "--N_batch") {
	  if (i + 1 < argc) { 
	    p->N_batch = stoi(argv[i+1]);
	    i++;
	  } else {
	    std::cerr << "--N_batch option requires one argument." << std::endl;
	    exit(1);
	  }
	} else if (arg == "--omega") {
	  if (i + 1 < argc) { 
	    p->omega = stod(argv[i+1]);
	    i++;
	  } else {
	    std::cerr << "--omega option requires one argument." << std::endl;
	    exit(1);
	  }  
	} else if ((arg == "-v")||(arg == "--verbose")) {
	  p->verbose = true;
	  cout << "Verbose mode on." <<endl;
	} else if (arg == "--corr"){
 	  if (i + 1 < argc) { 
	    p->random = stod(argv[i+1]);
	    p->corr = true;
	    i++;
	  } else {
	    std::cerr << "--corr option requires one argument." << std::endl;
	    exit(1);
	  }
	} else if ((arg == "-o")||(arg == "--output")) {
	  cout << "Output directory specified." <<endl;
	  if (i + 1 < argc) {
	    p->output = p->output + argv[i+1] + "/";
	    i++;
	    struct stat buffer;
	    if (stat((p->output).c_str(), &buffer) != 0){
	      cout << "Directory " << p->output << " does not exist !"  << endl;
	      fileError();
	    }
	  } 


	  else {
	    std::cerr << "--output option requires one argument." << std::endl;
	    exit(1);
	  }
	} else if ((arg == "-f")||(arg == "--from_file")) {
	  cout << "Loading network from file." <<endl;
	  p-> from_file = true;
	  if (i + 1 < argc) {
	    p->file_net =  argv[i+1];
	    p->file_net = "../data/" + p->file_net;
	    i++;
	    struct stat buffer;
	    if (stat((p->file_net).c_str(), &buffer) != 0){
	      cout << "Directory " << p->file_net << " does not exist !"  << endl;
	      fileError();
	    }
	  }
	}else if ((arg == "-t")||(arg == "--num_threads")) {
	  if (i + 1 < argc) {
	    p->num_threads = stoi(argv[i+1]);
	    i++;
	  } else {
	    std::cerr << "--num_threads option requires one argument." << std::endl;
	    exit(1);
	  }
	} else if (arg == "--A_only") {
	  p->A_only = true;
	} else if (arg == "--B_only") {
	  p->B_only = true;
	} else {
	  cout << "Invalid argument." <<endl;
	  show_usage(argv[0]);
	  exit(1);
        }
      }
      if (p->bott_map_bool){
	vector<std::string> out;
	tokenize(arg_bott_map, s, out);
	if (out.size() != 3) { cout << "Error while parsing bott_map." << endl; exit(1);}
	p->delta_min = stod(out[0]);
	p->delta_max = stod(out[1]);
	p->delta_step = stod(out[2]);        
      }
      if (p->omega_r_bool){
	vector<std::string> out;
	tokenize(arg_omega_r, s, out);
	if (out.size() != 6) { cout << "Error while parsing omega_r." << endl; exit(1);}
	p->omega_inf = stod(out[0]);
	p->omega_sup = stod(out[1]);
	p->omega_step = stod(out[2]);
	p->r_inf = stod(out[3]);
	p->r_sup = stod(out[4]);
	p->r_step = stod(out[5]);        
      }
      ofstream f_params(p->output + "params.dat");
      if (f_params){
	int N_atoms = 3*(p->N)*(p->N - 1) + 1;
	f_params << p->N << endl << p->a <<endl << p->delta_B << endl << p->delta_AB <<endl << round((p->random))/100. << endl << p->N_batch << endl << p->omega << endl;}
    else {fileError();}
    f_params.close();
}

void tokenize(std::string const &str, const char delim, std::vector<std::string> &out){
    size_t start;
    size_t end = 0;
 
    while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}	    

void saveNatoms(Params *p){
  ofstream f_params(p->output + "params.dat", std::ios_base::app);
  f_params << p->N_atoms << endl;
  f_params.close();
}


MatrixXcd generalProcedureFromFile(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, double random){

  Vector2d  *hexlattice;
  int dim = 0;
  int q = 0;
  string line;
  ifstream f_coord_count(p.file_net);
  if (f_coord_count.is_open()){
    while (getline(f_coord_count, line)){
      q++;
    }
    f_coord_count.close();
  } else {
    fileError();
  }
  p.N_atoms = q;
  cout << p.N_atoms <<endl;
  MatrixXcd G(2*p.N_atoms, 2*p.N_atoms);
  hexlattice = new Vector2d[p.N_atoms];
  generateG2D2(&p, hexlattice, &G);
  cout << "G generated" <<endl;
  ComplexEigenSolver<MatrixXcd> eigensolver(G);
  cout << "G diagonalized" <<endl;
  //cout << G <<endl;
  if (eigensolver.info() != Success) abort();
  MatrixXcd Q(2*p.N_atoms, 2*p.N_atoms);
  Q = eigensolver.eigenvectors();

  complex<double> eigenvalues[2*p.N_atoms];
  for (int i = 0; i < 2*p.N_atoms; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
  q = 0;
    cout << "eigenvalues filled" <<endl;
    	cout << p.N_atoms << endl;
	//  hexlattice = new Vector2d[p.N_atoms];
  // ifstream f_coord(p.file_net);
  // char comma = ',';
  // if (f_coord.is_open()){
  //   while (getline(f_coord, line)){

  //     vector<std::string> xy;
  //     tokenize(line.c_str(), comma, xy);

  //     hexlattice[q](0) = stod(xy[0]);
  //     hexlattice[q](1) = stod(xy[1]);
  //     q++;
  //   }
  //   f_coord.close();
  // } else {
  //   fileError();
  // }
    cout << "hexlattice loaded" <<endl;
        	cout << p.N_atoms << endl;
  eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
  eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
      cout << "freqs, gammas loaded" <<endl;
      p.N_atoms = q;
          	cout << p.N_atoms << endl;
		cout << freqs[0] << endl;
  sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
        cout << "sorted" <<endl;
	cout << hexlattice[0](0) << endl;
	cout << p.N_atoms << endl;
  computeUXUY(UX, UY, hexlattice, p.N_atoms);
  computeVXVY(UX, UY, VX, VY, &Q, p.N_atoms);
          cout << "UX, UY etc " <<endl;
  return Q;
}

bool inTheVector(double e1, double e2, vector<double> l1, vector<double> l2){
  for(int i = 0; i < l1.size(); i++){
    if (abs(e1-l1[i]) < 1e-2 and abs(e2-l2[i]) < 1e-2){
      return true;
    }
  }
  return false;
}

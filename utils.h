#ifndef COMPUTECACHE
#define COMPUTECACHE

#include <iostream>
#include <complex>
#include <Eigen/Dense>
#include <vector>
#include <sys/stat.h>

#include "bott.h"
#include "generateG.h"
#include "generateG_hex.h"
#include "modes.h"
#include "omp.h"
#include "Params.h"

using namespace std;
using namespace Eigen;

double omegaPractical(double *freqs, double omega, int *ivector, int N_atoms);

MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY);
MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, double delta_B, double delta_AB);
MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, Vector2d *hexlattice);
MatrixXcd generalProcedure(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, double random);
MatrixXcd generalProcedureFromFile(Params p, double *freqs, double *gammas, MatrixXcd *UX, MatrixXcd *UY,MatrixXcd *VX, MatrixXcd *VY, double random);


void fileError();
void show_usage(std::string name);
void parser(int argc, char **argv, Params *p);
void tokenize(std::string const &str, const char delim,std::vector<std::string> &out);
void saveNatoms(Params *p);

bool inTheVector(double e1, double e2, vector<double> l1, vector<double> l2);

#endif 

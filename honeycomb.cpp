#include "honeycomb.h"
#include <vector>
#include "utils.h"
#include "corr.h"
#include "Params.h"
#include "generateG_hex.h"

//#define EIGEN_DONT_PARALLELIZE
/* Parallelization made by eigen creates only small improvements (less
   than 1%) in performance and I don't want it to interfere with my
   own parallelization */

int main(int argc, char **argv)
{
  cout << "Honeycomb: v 6.14b" << endl;
  double t0 = omp_get_wtime();
  double t0_cpu = clock();
  Eigen::setNbThreads(4);
  int NN = 0, ivector = 0;

  // vérifier les ouvertures de fichier !!!
  
  Params p;
  parser(argc, argv, &p);

  omp_set_num_threads(p.num_threads);
  
  NN = p.N*p.N; 

    
    if (p.a != 1){cout << "a!=1 not supported in generateG (randomness incorrect)" <<endl;}
    if (p.bott_map_bool){
      if (p.verbose) cout << "Compute the bott map with omega = " << p.omega << " and w = " << p.random/p.a << "\nThe number of batch is:" << p.N_batch <<endl;
      for(int i = 0; i < p.N_batch; i++)
	computeMapBott(p, i);
    } else if (p.omega_r_bool){
      if (p.verbose) cout << "Compute the omega/w map with delta_B = " << p.delta_B << " and delta_AB = " << p.delta_AB << "\nThe number of batch is:" << p.N_batch <<endl;
      for(int i = 0; i < p.N_batch; i++)
	computeOmegaR(p, i);
    } else if (p.corr) {
      #pragma omp parallel for
      for(int i = 0; i < p.N_batch; i++)
	// paralleliser et changer le paramètre de corr doit recevoir rsup et omega
	// et préciser via --omega
	computeCorr(p, i); 
    } else if (p.from_file) {
  
    }   else {
      if (p.verbose) cout << "Generate the lattice ..." << endl;
      Vector2d  *hexlattice;
      hexlattice = new Vector2d[2*NN];
      double freqs[4*NN], gammas[4*NN];
      
      MatrixXcd Q(4*NN, 4*NN);
      MatrixXcd UX(2*NN, 2*NN), UY(2*NN, 2*NN), VX(4*NN, 4*NN), VY(4*NN, 4*NN);
      UX = MatrixXcd::Zero(2*NN, 2*NN);
      UY = MatrixXcd::Zero(2*NN, 2*NN);
      VX = MatrixXcd::Zero(4*NN, 4*NN);
      VY = MatrixXcd::Zero(4*NN, 4*NN);
      if (p.verbose) cout << "Diagonalize G, compute VX and VY ..." << endl;
      Q = generalProcedure(p, freqs, gammas, &UX, &UY, &VX, &VY, hexlattice);

  // // // computing bott map temporary deactivated 
      double omega_practical = omegaPractical(freqs, p.omega, &ivector, p.N_atoms);
      double ipr_m = ipr(Q, NN, ivector);
      if (p.verbose){
	cout << "m: " << ivector <<endl;
	cout << "omega: " << omega_practical <<endl;
	cout << "IPR_m: " << ipr_m << endl;
      }
      ofstream f_ipr(p.output + "ipr.dat");
      if (f_ipr) f_ipr << p.omega << "," << p.random << "," << ipr_m <<endl; else fileError();

      ofstream f_ipr_complex(p.output + "ipr_complex.dat");
      f_ipr_complex << "real,imag,IPR" <<endl;
      int ind_delocalized = 0;
      // for(int i = 0; i < 4*NN; i++){
      // 	f_ipr_complex << freqs[i] << "," << gammas[i] << "," << ipr(Q, NN, i)<<endl;
      // 	if (ipr(Q, NN, i) > 10./(2*NN)) {//cout << "mode délocalisé: " << freqs[i] << endl;
      // 	  ind_delocalized = i;
      // 	  intensityMap(Q, freqs, hexlattice, ind_delocalized, NN);
      // 	}
      // }
      f_ipr_complex.close();

      if (p.verbose) cout << "ComputeIntensity Map with omega = " << omega_practical << endl;
      intensityMap(Q, freqs, hexlattice, ivector, NN);

      ofstream f_bott(p.output + "bott.dat");
      double t0_bott = omp_get_wtime();
      double t0_cpu_bott = clock();
      if (f_bott){
	f_bott << bott(freqs, omega_practical, &VX, &VY) << "," << p.omega <<endl;
      } else {fileError();}
      double t1_bott = omp_get_wtime();
      double t1_cpu_bott = clock();
      cout << "Temps reel (bott): " << t1_bott-t0_bott << endl;
      cout << "Temps CPU (bott): " << (t1_cpu_bott-t0_cpu_bott)/CLOCKS_PER_SEC << endl;
      f_bott.close();


      // Remove borders
      omega_practical = omegaPractical(freqs, p.omega, &ivector, p.N_atoms);
      int tmp;
      double border = 0;
      // for(int i = 0; i < 2*NN; i++){
      // 	if ((i < p.N) or (i > 2*NN-p.N-1) or (i%p.N ==0) or ((i+1)%p.N == 0) ){
      // 	  border += norm(Q.col(ivector)(i));
      // 	}
      // }
      
      //loading borders of the hexagon

      
      ofstream f_border(p.output + "border.dat");
      f_border << "ivector" << "," << "f" << "," << "border" <<endl;
      for(int ivector = 0; ivector < 4*NN; ivector++){
	border = 0;
	for(int i = 0; i < 2*NN; i++){
	  if ((i < p.N) or (i > 2*NN-p.N-1) or (i%p.N == 0) or ((i+1)%p.N == 0) ){
	    border += norm(Q.col(ivector)(2*i));
	    border += norm(Q.col(ivector)(2*i+1));
	  }
	}
	f_border << ivector << "," << freqs[ivector] << "," << border << endl;
      }
      f_border.close();
      //      ofstream f_freq3("freq2.dat"); //This removes freq.dat !



      // else {fileError();}
      // f_freq2.close();
	

     
    }

    saveNatoms(&p);
  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  if (p.verbose){
    cout << "Temps reel  (main): " << t1-t0 << endl;
    cout << "Temps CPU (main): " << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;
  }
  return 0;
}


#include "Params.h"

using namespace std;

Params::Params(){
  N = 10;
  N_atoms = 2*N*N;
  N_batch = 1;
  num_threads = 2;
  a = 1;
  delta_B = 12;
  delta_AB = 0;
  omega = 0;
  r = 0;
  delta_min = 0;
  delta_max = 12;
  delta_step = 1;
  omega_inf = -5;
  omega_sup = 15;
  omega_step = 1;
  r_inf = 0;
  r_sup = 0;
  r_step = 1;
  verbose = false;
  omega_r_bool = false;
  bott_map_bool = false;
  corr = false;
  A_only = false;
  B_only = false;
  from_file = false;
  output = "../data/";
}
Params::~Params(){
}

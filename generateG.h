#include <iostream>
#include <complex>
#include <complex.h> // for cexp
#include <Eigen/Dense>
#include <sstream>
#include "Params.h"

using namespace std;
using namespace Eigen;

complex<double> P(complex<double> x);
complex<double> Q(complex<double> x);
void generateG2D(Params *p, Vector2d *hexlattice, MatrixXcd *G2);
void generateHex(Vector2d * hexlattice, int N, double a);
void generateHex2(Vector2d * hexlattice, int N, double a);
void generateHex3(Vector2d * hexlattice, Vector2d * hexlattice2, int N, double a);
int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy);

#include "randE.h"

#define EIGEN_DONT_PARALLELIZE
/* Parallelization made by eigen creates only small improvements (less
   than 1%) in performance and I don't want it to interfere with my
   own parallelization */

#include <ctime>

using namespace std;
using namespace Eigen;

int main(int argc, char **argv)
{
  cout << "randE: v 5.29b" << endl;
  
  double t0 = omp_get_wtime();
  double t0_cpu = clock();

  Params p;
  parser(argc, argv, &p);
  omp_set_num_threads(p.num_threads);

  MatrixXcd G(2*p.N_atoms,2*p.N_atoms);
  Vector2d  *hexlattice;
  hexlattice = new Vector2d[p.N_atoms];
  vector<string> data_ipr, data_bott, data_dos;
  int q = 0;
  
  int start = (int) p.r_inf*(1./p.r_step);
  int end = (int) p.r_sup*(1./p.r_step);
  int start2 = (int) p.omega_inf*(1./p.omega_step);
  int end2 = (int) p.omega_sup*(1./p.omega_step);
    srand(time(nullptr));

  for(int i_batch = 0; i_batch < p.N_batch; i_batch++){
    cout << i_batch <<endl;
    int ivector_pos = 0;
    #pragma omp parallel for
    for(int r = start; r <= end; r++){
      //p.random = r;
      generateG2D(&p, hexlattice, &G);
      for(int i = 1; i < 2*p.N_atoms; i+= 1){
	G(i,i) += rand()/(RAND_MAX + 0.01)*r;
	G(i,i) -= rand()/(RAND_MAX + 0.01)*r;
      }
      ComplexEigenSolver<MatrixXcd> eigensolver(G);
      if (eigensolver.info() != Success) abort();
      
      MatrixXcd Q(2*p.N_atoms,2*p.N_atoms);
      Q = eigensolver.eigenvectors();

      complex<double> eigenvalues[2*p.N_atoms];
      double freqs[2*p.N_atoms];
      double gammas[2*p.N_atoms];
      for (int i = 0; i < 2*p.N_atoms; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
      q = 0;
  

    
      eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
      eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
      sortEigenvectors(&Q, freqs, gammas, p.N_atoms);

      // for(int ivector = 150; ivector < 300; ivector++)
      //   intensityMap2(Q, freqs, hexlattice, ivector, p.N_atoms);

      // return 0;

      // BOTT
      // MatrixXcd UX(p.N_atoms, p.N_atoms), UY(p.N_atoms, p.N_atoms), VX(2*p.N_atoms, 2*p.N_atoms), VY(2*p.N_atoms, 2*p.N_atoms);
      // UX = MatrixXcd::Zero(p.N_atoms, p.N_atoms);UY = MatrixXcd::Zero(p.N_atoms, p.N_atoms);
      // VX = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);VY = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);

      // computeUXUY(&UX, &UY, hexlattice, p.N_atoms);
      // computeVXVY(&UX, &UY, &VX, &VY, &Q, p.N_atoms);

       for(int omega = start2; omega <= end2; omega++){
      	int ivector = 0;
	/**
	//IPR

      
	double ipr_m = ipr(Q, p.N_atoms, ivector);
	if (ivector == ivector_pos){;}else{
	  data_ipr.push_back(to_string(omega*p.omega_step) + "," + to_string(r*p.r_step) + "," + to_string(ipr_m));
	  ivector_pos = ivector;
	  }**/
	//double omega_practical = omegaPractical(freqs, omega*p.omega_step, &ivector, p.N_atoms);	
	//data_bott.push_back(to_string(bott(freqs, omega_practical,&VX, &VY)) + "," + to_string(omega*p.omega_step) + "," + to_string(r*p.r_step));
	//DOS
	double dos = DOS(freqs, omega-p.omega_step/2., omega+p.omega_step/2., p.N_atoms);
	data_dos.push_back(to_string(dos) + "," +  to_string(omega*p.omega_step) + "," + to_string(r*p.r_step));
	}
    }
  
  ofstream f_bott(p.output + to_string(i_batch) + "_botts.dat");
  ofstream f_dos(p.output + to_string(i_batch) + "_dos.dat");
  ofstream f_ipr(p.output + to_string(i_batch) + "_iprs.dat");
  f_ipr << "omega,err,IPR"<<endl;
  f_bott << "CB,omega,err" <<endl;
  f_dos << "DOS,omega,err"<<endl;
  
  for(size_t i = 0; i < data_dos.size(); ++i) {
    //f_bott << data_bott[i] << "\n";
    f_dos << data_dos[i] << "\n";
    }
  for(size_t i = 0; i < data_ipr.size(); ++i) {
    //f_ipr << data_ipr[i] << "\n";
  }
  f_bott.close(); f_ipr.close(); f_dos.close();

  }

  saveNatoms(&p);
  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  if (true){
    cout << "Temps reel (main): " << t1-t0 << endl;
    cout << "Temps CPU (main): " << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;
  }
}


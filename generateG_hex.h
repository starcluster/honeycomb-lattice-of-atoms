#include <iostream>
#include <complex>
#include <complex.h> // for cexp
#include <Eigen/Dense>
#include <sstream>
#include "generateG.h"
#include "Params.h"

using namespace std;
using namespace Eigen;

void generateG2D2(Params *p, Vector2d *hexlattice, MatrixXcd *G2);

void generateHex3(Vector2d * hexlattice, Vector2d * hexlattice2, int N, double a);

int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy);

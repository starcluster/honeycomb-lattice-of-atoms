#ifndef DEF_HEXGRID
#define DEF_HEXGRID

#include <Eigen/Dense>
#include <utility>
#include <iostream>

//This class is kept as a ressource it has no utility in the final
//version

class HexGrid{
 public:
  HexGrid(int size_of_grid, double size_of_step);
  ~HexGrid();

  Eigen::Vector2d get(int i); // get the ieth atom of the grid
  Eigen::Vector2d get(int i, int j); // get the (i,j) atom
  void print();
  void print_A();
  void print_B();
 private:
  double a;
  int N;
  
  Eigen::Vector2d a1, a2, a3, vertical;
  Eigen::Vector2d *Grid;

};

#endif

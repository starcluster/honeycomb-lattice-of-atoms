#ifndef DEF_PARAMS
#define DEF_PARAMS

#include <iostream>
#include <string>

class Params{
 public:
  Params();
  ~Params();
  int N, NN, N_batch, num_threads, N_atoms;
  double a, delta_B, delta_AB, omega, random, delta_min, delta_max, delta_step, omega_inf, omega_sup, omega_step, r_inf, r_sup, r_step, r;
  bool verbose, omega_r_bool, bott_map_bool, corr, A_only, B_only, from_file;
  std::string output, file_net;

};

#endif

//useless but still an interesting piece of code

void generateHex2(Vector2d * hexlattice, int N, double a){
  /*
    Adapted from https://stackoverflow.com/questions/2049196/generating-triangular-hexagonal-coordinates-xyz
and https://stackoverflow.com/questions/2459402/hexagonal-grid-coordinates-to-pixel-coordinates
   */
  int NN = N*N;
  std::vector<double> r,g,b;
  std:vector<bool> AB;
  bool current = true;

  for(int i = 0; i < N; i++){
    current = !current;
    double x = 0;
    double y = -i;
    double z = +i;
    r.push_back(x);
    g.push_back(y);
    b.push_back(z);
    AB.push_back(current);
    for(int j = 0; j < i; j++){
        x = x+1;
        z = z-1;
	r.push_back(x);
	g.push_back(y);
	b.push_back(z);
	AB.push_back(current);
      }

    for(int j = 0; j < i; j++){
	y = y+1;
	z = z-1;
	r.push_back(x);
	g.push_back(y);
	b.push_back(z);
	AB.push_back(current);
      }
    for(int j = 0; j < i; j++){
        x = x-1;
        y = y+1;
	r.push_back(x);
	g.push_back(y);
	b.push_back(z);
	AB.push_back(current);
      }
    for(int j = 0; j < i; j++){
      x = x-1;
      z = z+1;
      r.push_back(x);
      g.push_back(y);
      b.push_back(z);
      AB.push_back(current);
    }
    
    for(int j = 0; j < i; j++){
      y = y-1;
      z = z+1;
      r.push_back(x);
      g.push_back(y);
      b.push_back(z);
      AB.push_back(current);
    }

    for(int j = 0; j < i-1; j++){
      x = x+1;
      y = y-1;
      r.push_back(x);
      g.push_back(y);
      b.push_back(z);
      AB.push_back(current);
    }
  }
  cout << "r size " << r.size() << endl;
  for(int i = 0; i < r.size(); i++){
    double coord_x, coord_y;
    coord_y = 3./2.*a*b[i];
    coord_x = sqrt(3)*a*(b[i]/2+r[i]);
    int coord_y_int = (int) (coord_y*100);
    //    cout << coord_y_int <<endl;
    hexlattice[i](0) = coord_x;
    hexlattice[i](1) = coord_y;
    // if (coord_y_int%100==0)
    //   cout << coord_x << "," << coord_y << ","  << 0 <<endl;
    // else
    //   cout << coord_x << "," << coord_y << ","  << 1 <<endl;
  }
}

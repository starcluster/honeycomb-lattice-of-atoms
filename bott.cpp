#include "bott.h"


void computeUXUY(MatrixXcd *UX, MatrixXcd *UY, Vector2d *hexlattice, int N_atoms){
  double Lx, Ly;

  Lx = hexlattice[N_atoms-1](0) - hexlattice[0](0);
  Ly = hexlattice[N_atoms-1](1) - hexlattice[0](1);
  
  MatrixXd X(N_atoms, N_atoms), Y(N_atoms, N_atoms);
  cout << "X Y " <<endl;
  for(int i = 0; i < N_atoms; i++){
    X(i,i) = hexlattice[i](0);
    Y(i,i) = hexlattice[i](1);
  }
  for(int i = 0; i < N_atoms; i++){
    complex<double> ex = 0, ey = 0;
    ex = cos(2*M_PI*X(i,i)/Lx) + 1.0id*sin(2*M_PI*X(i,i)/Lx);
    ey = cos(2*M_PI*Y(i,i)/Ly) + 1.0id*sin(2*M_PI*Y(i,i)/Ly);
    (*UX)(i,i) = ex;
    (*UY)(i,i) = ey; 
  }
}

void computeVXVY(MatrixXcd *UX, MatrixXcd *UY, MatrixXcd *VX, MatrixXcd *VY, MatrixXcd *Q, int N_atoms){

    complex<double> coeff_x = 0, coeff_y = 0;
    for(int m = 0; m < 2*N_atoms; m++){
      for(int n = 0; n < 2*N_atoms; n++){
	coeff_x = 0; coeff_y = 0;
	for(int alpha = 0; alpha < N_atoms; alpha++){
	  coeff_x += (conj((*Q).col(m)(2*alpha))*(*Q).col(n)(2*alpha) + conj((*Q).col(m)(2*alpha+1))*(*Q).col(n)(2*alpha+1))*(*UX)(alpha, alpha);
	  coeff_y += (conj((*Q).col(m)(2*alpha))*(*Q).col(n)(2*alpha) + conj((*Q).col(m)(2*alpha+1))*(*Q).col(n)(2*alpha+1))*(*UY)(alpha, alpha);
	}
	(*VX)(m,n) = coeff_x;
	(*VY)(m,n) = coeff_y;
      }
    }
}
double bott(double *freqs, double omega, MatrixXcd *VX, MatrixXcd *VY){
  int k = 0;
  MatrixXcd VXk(k,k), VYk(k,k);
  
  while(freqs[k] < omega)
    k++;

  VXk = (*VX).block(0,0,k,k);
  VYk = (*VY).block(0,0,k,k);

  ComplexEigenSolver<MatrixXcd> eig(VXk*VYk*VXk.adjoint()*VYk.adjoint(), false);
  complex<double> Tr = 0;
  for(int i = 0; i < eig.eigenvalues().rows(); i++){
    Tr += log(eig.eigenvalues()(i));
  }
  double to_ceil = imag(Tr)/2/M_PI;
  if (abs(to_ceil) < 1e-6) return 0;
  else return to_ceil;
}

void computeMapBott(Params p, int i_batch){
  double t0 = omp_get_wtime();
  double t0_cpu = clock();
  int NN = p.N*p.N;
  int ivector = 0;
  ofstream f_bott_init(p.output + to_string(i_batch) +  "_botts_map.dat");
  f_bott_init << "CB,deltaB,deltaAB" <<endl;
  f_bott_init.close();
  int start = (int)p.delta_min*(1./p.delta_step);
  int end = (int)p.delta_max*(1./p.delta_step);
  #pragma omp parallel for
  for(int delta_B = start; delta_B <= end; delta_B++){
      for(int delta_AB = start; delta_AB <= end; delta_AB++){
	  double freqs[4*NN], gammas[4*NN];
	  MatrixXcd Q(4*NN, 4*NN);
	  MatrixXcd UX(2*NN, 2*NN), UY(2*NN, 2*NN), VX(4*NN, 4*NN), VY(4*NN, 4*NN);
	  UX = MatrixXcd::Zero(2*NN, 2*NN);
	  UY = MatrixXcd::Zero(2*NN, 2*NN);
	  VX = MatrixXcd::Zero(4*NN, 4*NN);
	  VY = MatrixXcd::Zero(4*NN, 4*NN);
	  Q = generalProcedure(p, freqs, gammas, &UX, &UY, &VX, &VY, delta_B*p.delta_step, delta_AB*p.delta_step);
	  double omega_practical = omegaPractical(freqs, p.omega, &ivector, p.N_atoms);
	  ofstream f_bott(p.output + to_string(i_batch) +  "_botts_map.dat", ios_base::app);
	  if (f_bott){
	    f_bott << bott(freqs, omega_practical, &VX, &VY) << "," << delta_B*p.delta_step << "," << delta_AB*p.delta_step <<endl;
	  } else {fileError();}
	  f_bott.close();
      }
  }
  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  cout << "Temps reel (compute map bott): " << t1-t0 << endl;
  cout << "Temps CPU: (compute map bott)" << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;
}

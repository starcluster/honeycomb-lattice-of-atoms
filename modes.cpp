#include "modes.h"

void eigenvaluesToFreqs(complex<double> *U, double *V,  int N_atoms){
  for(int i = 0; i < 2*N_atoms; i ++){
    V[i] = -real(U[i])*0.5;
  }
}

void eigenvaluesToGammas(complex<double> *U, double *V,  int N_atoms){
  for(int i = 0; i < 2*N_atoms; i ++){
    V[i] = imag(U[i]);
  }
}

void sortEigenvectors(MatrixXcd *Q, double *freqs, double *gammas, int N_atoms){
  /*The folowing aims to sort eigenvectors according to
    the frequencies of normal modes*/  
  double swap1, swap2;
  int ind = 0, ind_inf = 0;

  ofstream f_freqs("../data/freq.dat");
  f_freqs << "f" << endl;
  
  for(int i = 0; i < 2*N_atoms; i++){
    ind_inf = ind + pos_minimum_vector(freqs, ind, N_atoms);
    swap1 = freqs[ind_inf];
    swap2 = gammas[ind_inf];
    
    freqs[ind_inf] = freqs[ind];
    gammas[ind_inf] = gammas[ind];
    
    freqs[ind] = swap1;
    gammas[ind] = swap2;
    
    Q->col(ind).swap(Q->col(ind_inf));
    ind++;
    f_freqs << freqs[i]  <<endl;
  }
  f_freqs.close();

}

int pos_minimum_vector(double *v, int indice, int N_atoms){
  int ind = indice;
  double inf = v[indice];
  for(int i = indice+1; i < 2*N_atoms; i++){
    if (v[i] < inf) {
      inf = v[i];
      ind = i;
    }
  }
  return ind - indice;
}

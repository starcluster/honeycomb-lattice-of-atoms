#include <iostream>
#include <complex>
#include <complex.h> // for cexp
#include <Eigen/Dense>
#include <sstream>

#include "modes.h"
#include "utils.h"
#include "generateG.h"
#include "Params.h"

using namespace std;
using namespace Eigen;

double bott(double *freqs, double omega, MatrixXcd *VX, MatrixXcd *VY);
void computeUXUY(MatrixXcd *UX, MatrixXcd *UY, Vector2d *hexlattice, int N_atoms);
void computeVXVY(MatrixXcd *UX, MatrixXcd *UY, MatrixXcd *VX, MatrixXcd *VY, MatrixXcd *Q, int N_atoms);
void computeMapBott(Params p, int i_batch);

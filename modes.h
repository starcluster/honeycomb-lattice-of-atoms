#include <iostream>
#include <complex>
#include <Eigen/Dense>
#include <fstream>

using namespace std;
using namespace Eigen;

void eigenvaluesToFreqs(complex<double> *U, double *V,  int N_atoms);
void eigenvaluesToGammas(complex<double> *U, double *V,  int N_atoms);
void sortEigenvectors(MatrixXcd *Q, double *freqs, double *gammas, int N_atoms);
int pos_minimum_vector(double *v, int indice, int N_atoms);


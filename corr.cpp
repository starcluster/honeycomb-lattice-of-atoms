#include "corr.h"

double computeCorr(Params p, int i_batch){
  int NN = p.N*p.N;
  VectorXcd psi_0;
  double a = 1;
  double omega_t = p.omega;
  ofstream f_corr(p.output + to_string(i_batch) + "_corr.dat");
  f_corr << "r,corr"<<endl;
  double t0 = omp_get_wtime();
  double t0_cpu = clock();

  MatrixXcd Q(4*NN, 4*NN), UX(2*NN, 2*NN), UY(2*NN, 2*NN), VX(4*NN, 4*NN), VY(4*NN, 4*NN);
  double freqs[4*NN], gammas[4*NN];
  
  if (f_corr){
    int end = (int) (p.random*10); // to improve
    cout << end << endl;

    for(int r = 0; r <= end; r++){  
  
      double corr = 0;

      UX = MatrixXcd::Zero(2*NN, 2*NN);
      UY = MatrixXcd::Zero(2*NN, 2*NN);
      VX = MatrixXcd::Zero(4*NN, 4*NN);
      VY = MatrixXcd::Zero(4*NN, 4*NN);
      Q = generalProcedure(p, freqs, gammas, &UX, &UY, &VX, &VY, r/10.);
      int ivector;
      omega_t = omegaPractical(freqs, p.omega, &ivector, p.N_atoms);
      if (r == 0) psi_0 = Q.col(ivector);
      else {
	// 	VectorXcd psi_i = Q.col(ivector);
	// for(int i = 0; i < 4*NN; i++){
	//   corr += abs(psi_0(i)*conj(psi_i(i)));
	// }
	for(int j = 0; j < 4*NN; j++){
	  VectorXcd psi_i = Q.col(j);
	  for(int i = 0; i < 4*NN; i++){
	    corr += abs(psi_0(i)*conj(psi_i(i)));
	  }
	}
      }
      f_corr << r/100. << "," << corr/(4*NN) << endl;
    }
    
    f_corr.close();
  } else {fileError();}
  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  cout << "Temps reel (main): " << t1-t0 << endl;
  cout << "Temps CPU (main): " << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;
  
  return 1.;
}


#ifndef COMPUTEOMEGAR
#define COMPUTEOMEGAR
#include <iostream>
#include <complex>
#include <Eigen/Dense>
#include <sstream>

#include "bott.h"
#include "generateG.h"
#include "modes.h"
#include "utils.h"
#include "omp.h"
#include "ipr.h"
#include "Params.h"

using namespace std;
using namespace Eigen;

void computeOmegaR(Params p, int i_batch);
double DOS(double *freqs, double omega1, double omega2, int N_atoms);
#endif

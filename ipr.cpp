#include "ipr.h"

double ipr(MatrixXcd Q, int N_atoms, int m){
  /* Computes inverse participation ratio of the mth quasimode
     (quantifies spatial localization of the mode) */
  double ipr_m = 0, mod_sqr = 0;
  complex<double> vec_m[2*N_atoms];
  for(int i = 0; i < 2*N_atoms; i++){
    vec_m[i] = Q.col(m)(i);
  }
  for(int i = 0; i < N_atoms; i++){
    mod_sqr = norm(vec_m[2*i]) + norm(vec_m[2*i+1]);
    ipr_m += mod_sqr*mod_sqr;
  }
  return ipr_m;
}


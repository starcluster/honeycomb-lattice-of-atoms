#include "generateG_hex.h"
#include <fstream>
#include "utils.h"

using namespace std;
using namespace Eigen;

void generateG2D2(Params *p, Vector2d *hexlattice, MatrixXcd *G2){
  const double k_0 = 2*M_PI*0.05/p->a;
  //  const    int N_atoms = 3*N*(N-1) + 1;
  int N_atoms = 0;
  int NN = p->N*p->N;
  int sgn;
  
  double r, dx, dy, sr1_2 = 1.0/sqrt(2), deltaR, deltaPhi, deltaX, deltaY;
  complex<double> tmp1, tmp2, tmp3;

  Matrix2cd deg, block;

  deg << 1./sqrt(2), 1.0id*sr1_2, -1./sqrt(2), 1.0id*sr1_2;

  // --> generate the hex network: sublattices and total lattice
  //generateHex3(hexlattice, hexlattice2, N, a);
  // cheat:
  int q = 0;
  string line;

  char comma = ',';
  
  //  hexlattice = new Vector2d[p->N_atoms];
  q = 0;
  ifstream f_coord(p->file_net);
  if (f_coord.is_open()){
    while (getline(f_coord, line)){

      vector<std::string> xy;
      tokenize(line.c_str(), comma, xy);

      hexlattice[q](0) = stod(xy[0]);
      hexlattice[q](1) = stod(xy[1]);
      q++;
    }
    f_coord.close();
  } else {
    fileError();
  }


    if (p->A_only){
    for(int i = 0; i < p->N_atoms/2; i++){ // FACTORIZE !!
      deltaR = rand()/100/(RAND_MAX + 0.01)*p->random;
      deltaPhi = rand()/(RAND_MAX + 0.01)*2*M_PI;
      
      deltaX = deltaR*cos(deltaPhi);
      deltaY = deltaR*sin(deltaPhi);
      hexlattice[i](0) += deltaX;
      hexlattice[i](1) += deltaY;
    }
  } else if (p->B_only) {
        for(int i = p->N_atoms/2; i < p->N_atoms; i++){ 
	  deltaR = rand()/100/(RAND_MAX + 0.01)*p->random;
	  deltaPhi = rand()/(RAND_MAX + 0.01)*2*M_PI;
	  
	  deltaX = deltaR*cos(deltaPhi);
	  deltaY = deltaR*sin(deltaPhi);
	  hexlattice[i](0) += deltaX;
	  hexlattice[i](1) += deltaY;
	}
  } else {
      for(int i = 0; i < p->N_atoms; i++){ 
      deltaR = rand()/100/(RAND_MAX + 0.01)*p->random;
      deltaPhi = rand()/(RAND_MAX + 0.01)*2*M_PI;
      
      deltaX = deltaR*cos(deltaPhi);
      deltaY = deltaR*sin(deltaPhi);
      hexlattice[i](0) += deltaX;
      hexlattice[i](1) += deltaY;
    }
  }
  
  int c = 0;

  MatrixXcd G(2*p->N_atoms, 2*p->N_atoms);
  //cout << "f loaded " << p->N_atoms << endl;
  
  for(int i = 0; i < p->N_atoms; i++){
    for(int j = 0; j < p->N_atoms; j++){ // we fill the matrix by 2x2 blocks
      if (i==j){
	Matrix2cd O;
	O << 0,0,0,0;
	G.block(i,j,2,2) =  O; // ??
      } else {
	dx = k_0*p->a*(hexlattice[i](0) - hexlattice[j](0));
	dy = k_0*p->a*(hexlattice[i](1) - hexlattice[j](1));
	r = sqrt(dx*dx + dy*dy);
	if (r==0){ cout << "nooooon " << dx << " " << dy <<  "  " <<endl;
	  cout << hexlattice[i] << endl;
	  cout << hexlattice[j] <<endl;	}
	//double abc;
	//cin >> abc;
	tmp1 = 3./2.*cexp(1.0id*r)/r;
	tmp2 = P(1.0id*r);
	tmp3 = Q(1.0id*r);
	G(2*i, 2*j) = tmp1*(tmp2+tmp3*dx*dx/(r*r));
	G(2*i, 2*j + 1) = tmp1*(tmp3*dx*dy/(r*r));
	G(2*i + 1, 2*j) = tmp1*(tmp3*dx*dy/(r*r));
	G(2*i + 1, 2*j + 1) = tmp1*(tmp2+tmp3*dy*dy/(r*r));
      }
    }
  }
  //cout << G << endl;  
  for(int i = 0; i < p->N_atoms; i++){
    for(int j = 0; j < p->N_atoms; j++){ // we fill the matrix by 2x2 blocks
      if (i==j){
	// int coord_y_int = (int) (hexlattice[i](1)*100);
	// if (coord_y_int%100) { sgn = 1; } else {sgn = -1;}
	if (i < p->N_atoms/2) { sgn = -1; } else {sgn = 1;}
	G(2*i, 2*j) = 1.0id + 2*p->delta_B + 2*sgn*p->delta_AB;
	G(2*i + 1, 2*j + 1) = 1.0id - 2*p->delta_B + 2*sgn*p->delta_AB;
	G(2*i+1, 2*j) = 0;
	G(2*i, 2*j+1) = 0;
      } else {
	block = G.block(2*i,2*j,2,2);
	G.block(2*i,2*j,2,2) = deg*block*deg.adjoint();
      }
    }
  }

  //cout << G <<endl;
  for(int i = 0; i < 2*p->N_atoms; i++){
    for(int j = 0; j < 2*p->N_atoms; j++){
      //      cout << G(i,j) << " " << i << " " << j << endl;
      (*G2)(i,j) = G(i,j);
    }
  }
}

void generateHex3(Vector2d * hexlattice, Vector2d * hexlattice2, int N, double a){
  generateHex(hexlattice, N, a); //N_atomes has to be cohrent everywhere
  int NN = N*N;
  double hex_length = 32; //arbitrary defined so far
  double m = 11;
  int N_atoms = 3*N*(N-1) +1;
  float vertx[8];
  float verty[8];
  // vertx[0] = 0; verty[0] = hex_length*sqrt(3)/2.;
  // vertx[1] = hex_length*0.5; verty[1] = 0 + 0.5;
  // vertx[2] = hex_length*1.5 - 0.1; verty[2] = 0 + 0.5;
  // vertx[3] = hex_length*2 - 0.5; verty[3] = hex_length*sqrt(3)/2.;
  // vertx[4] = hex_length*1.5 - 0.5; verty[4] = hex_length*sqrt(3);
  // vertx[5] = hex_length*0.5; verty[5] = hex_length*sqrt(3);

  // vertx[0] = 0; verty[0] = hex_length*sqrt(3)/2.;
  // vertx[1] = hex_length*0.5-0.1; verty[1] = 0.1;
  // vertx[2] = hex_length*0.5+0.5; verty[2] = 0.1;
  // vertx[3] = hex_length*1; verty[3] = hex_length*sqrt(3)/2.-0.9;
  // vertx[4] = hex_length*1+0.6; verty[4] = hex_length*sqrt(3);
  // vertx[5] = hex_length*0.5+0.6; verty[5] = hex_length*sqrt(3)*3/2-0.3;
  // vertx[6] = hex_length*0.5+0.1; verty[6] = hex_length*sqrt(3)*3/2-0.3;
  // vertx[7] = hex_length*0; verty[7] = hex_length*sqrt(3);

  // vertx[0] = 0; verty[0] = 20; //20 arbitrary
  // vertx[1] = hex_length * sqrt(3)/2.; verty[1] = 20 - hex_length/2.;
  // vertx[2] = hex_length*sqrt(3); verty[2] = 20;
  // vertx[3] = hex_length*sqrt(3); verty[3] = 20 + hex_length/2.;
  // vertx[4] = hex_length*sqrt(3)/2.; verty[4] = 20 + hex_length;
  // vertx[5] = 0; verty[5] = 20 + hex_length/2.;

  double pi = 3.141592;
  
  double xstep = a*sqrt(3);
  double xmax = (m+0.5)*xstep;
  double xmin = (-m+0.5)*xstep;
  double ymax = xmax*tan(pi/6.);
  double ymin = -ymax + a;
  double x0 = 0.5*xstep;
  double y0max = 2*ymax - a/2;
  double y0min = 2*ymin - a/2;

  double shiftx = 30*x0;
  double shifty = 0;

  vertx[0] = xmin + shiftx-0.1; verty[0] = ymin + shifty; //20 arbitrary
  vertx[1] = x0 + shiftx; verty[1] = y0min + shifty;
  vertx[2] = xmax + shiftx; verty[2] = ymin + shifty;
  vertx[3] = xmax + shiftx; verty[3] = ymax + shifty;
  vertx[4] = x0 + shiftx; verty[4] = y0max + shifty;
  vertx[5] = xmin + shiftx; verty[5] = ymax + shifty;


  vertx[6] = hex_length*0.5+0.1; verty[6] = hex_length*sqrt(3)*3/2-0.3;
  vertx[7] = hex_length*0; verty[7] = hex_length*sqrt(3);  

  int dim = 0;
  
  for(int i = 0; i < 2*NN; i++){
    if (pnpoly(6, vertx, verty, hexlattice[i](0), hexlattice[i](1)) == 1){
      //cout << hexlattice[i](0) << "," << hexlattice[i](1) << ",1" <<endl;
      hexlattice2[dim] = hexlattice[i];
      dim++;
    }
  }
  cout << "ceci est dim " << dim <<endl;
}



int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
{
  //Using Jordan theorem : https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
  int i, j, c = 0;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
     (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}

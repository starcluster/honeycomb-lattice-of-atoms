#include "intensityMap.h"
#include <iostream>

void intensityMap(MatrixXcd Q, double *freqs, Vector2d *hexlattice, int ivector, int NN){
  double intensity;
  complex<double> x1, x2;
  Vector2cd v;
  ofstream  f_intensity_map("../data/" + to_string(ivector) + "_intensity_map.dat");
  f_intensity_map << "x,y,I,f" << endl;

  for(int i = 0; i < 4*NN; i+=2){
    x1 = Q.col(ivector)(i);
    x2 = Q.col(ivector)(i+1);
    v = Vector2cd(x1,x2);
    intensity = v.squaredNorm();
    f_intensity_map <<  hexlattice[i/2].transpose()(0) << "," << hexlattice[i/2].transpose()(1) << "," << intensity << "," << freqs[ivector] << endl;
  }  
  f_intensity_map.close();
}

void intensityMap2(MatrixXcd Q, double *freqs, Vector2d *hexlattice, int ivector, int N_atoms){
  double intensity;
  complex<double> x1, x2;
  Vector2cd v;
  ofstream  f_intensity_map("../data/" + to_string(ivector) + "_intensity_map.dat");
  f_intensity_map << "x,y,I,f" << endl;

  for(int i = 0; i < 2*N_atoms; i+=2){
    x1 = Q.col(ivector)(i);
    x2 = Q.col(ivector)(i+1);
    
    v = Vector2cd(x1,x2);
        intensity = v.squaredNorm();
    f_intensity_map <<  hexlattice[i/2].transpose()(0) << "," << hexlattice[i/2].transpose()(1) << "," << intensity << "," << freqs[ivector] << endl;
  }  
  f_intensity_map.close();
}



#include <iostream>
#include <complex>
#include <algorithm>           // std::for_each
#include <boost/format.hpp>    // only needed for printing
#include <boost/histogram.hpp> // make_histogram, regular, weight, indexed
#include <cassert>             // assert (used to test this example for correctness)
#include <functional>          // std::ref
#include <sstream>             // std::ostringstream
#include <fstream>
#include <string>
#include <boost/tokenizer.hpp>


using namespace std;
using namespace boost::histogram;
using namespace boost;



#include "parall.h"

#define EIGEN_DONT_PARALLELIZE
/* Parallelization made by eigen creates only small improvements (less
   than 1%) in performance and I don't want it to interfere with my
   own parallelization */

using namespace std;
using namespace Eigen;

int main(int argc, char **argv)
{
  cout << "Parall: v 6.16b" << endl;
  
  double t0 = omp_get_wtime();
  double t0_cpu = clock();

  Params p;
  parser(argc, argv, &p);
  omp_set_num_threads(p.num_threads);

  int q = 0;
  string line;
  
  ifstream f_coord_count(p.file_net);
  if (f_coord_count.is_open()){
    while (getline(f_coord_count, line)){
      q++;
    }
    f_coord_count.close();
  } else {
    fileError();
  }
  p.N_atoms = q;
  cout << " N_atoms = " << p.N_atoms <<endl;
  MatrixXcd G(2*p.N_atoms,2*p.N_atoms);
  Vector2d  *hexlattice;
  hexlattice = new Vector2d[p.N_atoms];
  
  int start = (int) p.r_inf*(1./p.r_step);
  int end = (int) p.r_sup*(1./p.r_step);
  int start2 = (int) p.omega_inf*(1./p.omega_step);
  int end2 = (int) p.omega_sup*(1./p.omega_step);

  // ofstream f_bott(p.output + "0_botts_map.dat");
  // f_bott << "CB,deltaB,deltaAB" <<endl;
  // #pragma omp parallel for
  // for(int deltaB = -12; deltaB <13; deltaB++){
  //   for(int deltaAB = -12; deltaAB < 13; deltaAB++){
  //     p.delta_B = deltaB;
  //     p.delta_AB = deltaAB;
  //     generateG2D2(&p, hexlattice, &G);
  //           ComplexEigenSolver<MatrixXcd> eigensolver(G);
  //     if (eigensolver.info() != Success) abort();
      
  //     MatrixXcd Q(2*p.N_atoms,2*p.N_atoms);
  //     Q = eigensolver.eigenvectors();

  //     complex<double> eigenvalues[2*p.N_atoms];
  //     double freqs[2*p.N_atoms];
  //     double gammas[2*p.N_atoms];
  //     for (int i = 0; i < 2*p.N_atoms; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
  //     eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
  //     eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
  //     sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
  //     MatrixXcd UX(p.N_atoms, p.N_atoms), UY(p.N_atoms, p.N_atoms), VX(2*p.N_atoms, 2*p.N_atoms), VY(2*p.N_atoms, 2*p.N_atoms);
  //     UX = MatrixXcd::Zero(p.N_atoms, p.N_atoms);UY = MatrixXcd::Zero(p.N_atoms, p.N_atoms);
  //     VX = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);VY = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);

  //     computeUXUY(&UX, &UY, hexlattice, p.N_atoms);
  //     computeVXVY(&UX, &UY, &VX, &VY, &Q, p.N_atoms);
  //     f_bott << bott(freqs, 7,&VX, &VY) << "," << deltaB << "," << deltaAB << endl;
  //   }
  // }
  // return 0;
  #pragma omp parallel for
  for(int i_batch = 0; i_batch < p.N_batch; i_batch++){
    vector<string> data_ipr, data_bott, data_dos;
    cout << "ibatch" << i_batch <<endl;
    int ivector_pos = 0;
    //#pragma omp parallel for
    for(int r = start; r <= end; r++){
      cout << r <<endl;
      p.random = r*p.r_step;
      generateG2D2(&p, hexlattice, &G);
      ComplexEigenSolver<MatrixXcd> eigensolver(G);
      if (eigensolver.info() != Success) abort();
      
      MatrixXcd Q(2*p.N_atoms,2*p.N_atoms);
      Q = eigensolver.eigenvectors();

      complex<double> eigenvalues[2*p.N_atoms];
      double freqs[2*p.N_atoms];
      double gammas[2*p.N_atoms];
      for (int i = 0; i < 2*p.N_atoms; i++) eigenvalues[i] = eigensolver.eigenvalues()(i);
      q = 0;
  

    
      eigenvaluesToFreqs(eigenvalues, freqs, p.N_atoms);
      eigenvaluesToGammas(eigenvalues, gammas, p.N_atoms);
      sortEigenvectors(&Q, freqs, gammas, p.N_atoms);
         // for(int ivector = 500; ivector < 600; ivector++)
       //   intensityMap2(Q, freqs, hexlattice, ivector, p.N_atoms);


      /**
      // BOTT
      MatrixXcd UX(p.N_atoms, p.N_atoms), UY(p.N_atoms, p.N_atoms), VX(2*p.N_atoms, 2*p.N_atoms), VY(2*p.N_atoms, 2*p.N_atoms);
      UX = MatrixXcd::Zero(p.N_atoms, p.N_atoms);UY = MatrixXcd::Zero(p.N_atoms, p.N_atoms);
      VX = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);VY = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);

      computeUXUY(&UX, &UY, hexlattice, p.N_atoms);
      computeVXVY(&UX, &UY, &VX, &VY, &Q, p.N_atoms);
      **/

            ifstream f_hex_b(p.output +  "hex_b.dat");
      
      int q = 0;
      string line;
      char comma = ',';
      vector<double> x_b, y_b;
      
      if (f_hex_b.is_open()){
	while (getline(f_hex_b, line)){

	  vector<std::string> xy;
	  tokenize(line.c_str(), comma, xy);

	  x_b.push_back(stod(xy[0]));
	  y_b.push_back(stod(xy[1]));
	  q++;
	}
      }
    f_hex_b.close();

    vector<int> lpos;

    for(int i = 0; i < p.N_atoms; i++){
      //cout << hexlattice[i][0] << "," << hexlattice[i][1] <<endl;
      //cout << x_b[i] <<endl;
      if (inTheVector(hexlattice[i][0], hexlattice[i][1], x_b, y_b)){
	lpos.push_back(i);
	// cout << hexlattice[i][0] << " " << hexlattice[i][1] <<endl;
		//cout << x_b[i] << " " << y_b[i] <<endl;
	//	cout << "c'est un bord ! "<< endl;
      } else { }//cout << "c'est pas un bord !" <<endl;}
    }
      
      for(int omega = start2; omega <= end2; omega++){
	int ivector = 0;
     
	//IPR
	double omega_practical = omegaPractical(freqs, omega*p.omega_step, &ivector, p.N_atoms);
      
	double ipr_m = ipr(Q, p.N_atoms, ivector);
	//if (ivector == ivector_pos){;}else{
	  data_ipr.push_back(to_string(omega*p.omega_step) + "," + to_string(r*p.r_step) + "," + to_string(ipr_m));
	  ivector_pos = ivector;

	  // Calcul du BORD
	  double edge = 0;
	  for(int i = 0; i < p.N_atoms; i++){
	    if (count(lpos.begin(), lpos.end(), i) == 1 ){
		    edge += norm(Q.col(ivector)(2*i));
		    edge += norm(Q.col(ivector)(2*i+1));
		  }
	  }
	  cout << edge << endl;
	  //}
	/**
	data_bott.push_back(to_string(bott(freqs, omega_practical,&VX, &VY)) + "," + to_string(omega*p.omega_step) + "," + to_string(r*p.r_step));
	**/
	//DOS
	double dos = DOS(freqs, omega*p.omega_step-p.omega_step/2., omega*p.omega_step+p.omega_step/2., p.N_atoms);
	if (edge > 0.3){
	  dos = 0;
	}
	data_dos.push_back(to_string(dos) + "," +  to_string(omega*p.omega_step) + "," + to_string(r*p.r_step));
	
      }
  
  ofstream f_bott(p.output + to_string(i_batch) + "_botts.dat");
  ofstream f_dos(p.output + to_string(i_batch) + "_dos.dat");
  ofstream f_ipr(p.output + to_string(i_batch) + "_iprs.dat");
  f_ipr << "omega,err,IPR"<<endl;
  f_bott << "CB,omega,err" <<endl;
  f_dos << "DOS,omega,err"<<endl;
  
  for(size_t i = 0; i < data_dos.size(); ++i) {
    //f_bott << data_bott[i] << "\n";
    f_dos << data_dos[i] << "\n";
    //cout << "ok" <<endl;
    }
  for(size_t i = 0; i < data_ipr.size(); ++i) {
      f_ipr << data_ipr[i] << "\n";
  }
  f_bott.close(); f_ipr.close(); f_dos.close();
    }
    data_dos.clear();
    data_ipr.clear();
    data_bott.clear();
  }

  double t1 = omp_get_wtime();
  double t1_cpu = clock();

  saveNatoms(&p);
  if (true){
    cout << "Temps reel (main): " << t1-t0 << endl;
    cout << "Temps CPU (main): " << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;
  }
}


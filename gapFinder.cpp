#include "gapFinder.h"

int main(){
  bool in_the_gap = false;
  int N = 0, NN = 0;
  double inf_bound = -6, sup_bound = 26, start = 0, end = 0, gap = 0;
  double delta  = sup_bound - inf_bound;

  int n_bins = (int) (delta);
  string s;
  
  ifstream f_params("../data/params.dat");
  getline(f_params, s);N = stoi(s); NN = N*N;
  double freqs[4*NN];
  
  ifstream f_freqs("../data/freq.dat");
   
  if (f_freqs){
    getline(f_freqs,s);
    int i = 0;
    while(getline(f_freqs, s)){
      tokenizer<escaped_list_separator<char> > tok(s);
      freqs[i] = stod(*(tok.begin()));
      i++;
    }
  } else { cout <<"Error: can't open file"<<endl; abort(); }

  auto h = make_histogram(axis::regular<>(n_bins, inf_bound, sup_bound, "x"));
  for(int i = 0; i < 4*NN; i++){
    h(freqs[i]);
  }

  for (auto&& x : indexed(h, coverage::all)) {
    //cout << (*x)/(4*NN) << "   " <<  1./(4*NN) <<endl;
    if ((*x)/(4*NN) <= 1./(4*NN) and !in_the_gap){
      start = x.bin().lower();
      in_the_gap = true;
    } else if ((*x)/(4*NN) <= 1./(4*NN) and in_the_gap) {
      end = x.bin().upper();
    } else if ((*x)/(4*NN) > 1./(4*NN) and in_the_gap) {
      end = x.bin().upper();
      in_the_gap = false;
    } else {;}
    if ((end - start) > gap) gap = end-start;
  }

  ostringstream os;
  for (auto&& x : indexed(h, coverage::all)) {
    os << boost::format("bin %2i [%4.1f, %4.1f): %i\n")
      % x.index() % x.bin().lower() % x.bin().upper() % *x;
  }
  
  //cout << os.str() << flush;
  // cout << endl;
  // looking for a peak in the interval 0:12
  double i1 = 0, i2 = 12;
  auto h2 = make_histogram(axis::regular<>(6, i1, i2, "x"));
  for(int i = 0; i < 4*NN; i++){
    h2(freqs[i]);
  }
  double total_gap = 0;
  for (auto&& x : indexed(h2, coverage::all)) {
    if(*x/(4*NN) > 4./(4*NN) and x.bin().lower()!=12.0 and x.bin().upper()!=0) {gap = 0;
    os << boost::format("bin %2i [%4.1f, %4.1f): %i\n")
      % x.index() % x.bin().lower() % x.bin().upper() % *x;
    }
  }
  cout << os.str() <<flush;
//if (total_gap/(4*NN) > 4./(4*NN)) gap = 0;

  ofstream f_gap("../data/gap.dat");
  f_gap << gap << endl;
  f_gap.close();
  //  cout << "GAP = " << gap << " ! ! !" <<endl;
  // cout << os.str() << flush;
  return 0;
}
  

from params import *

raw_ipr_B = pd.read_csv("bandeB12_p/" + "0_iprs.dat")
raw_ipr_AB = pd.read_csv("bandeAB12_p/" + "0_iprs.dat")

raw_ipr_B = raw_ipr_B.sort_values('omega')
raw_ipr_AB = raw_ipr_AB.sort_values('omega')

for i in range(1, N_batch):
    try:
        raw_ipr_B_tmp = pd.read_csv("bandeB12_p/" + str(i) + "_iprs.dat")
        raw_ipr_AB_tmp = pd.read_csv("bandeAB12_p/" + str(i) + "_iprs.dat")

        raw_ipr_B += raw_ipr_B_tmp.sort_values('omega')
        raw_ipr_AB += raw_ipr_AB_tmp.sort_values('omega')
        
    except FileNotFoundError:
        pass

print(N_batch)
plt.plot(raw_ipr_B['omega'], raw_ipr_B['IPR'], label='$\Delta_B=12$')
plt.plot(raw_ipr_AB['omega'], raw_ipr_AB['IPR'], label='$\Delta_{AB}=12$')

plt.title( "\n $N_{atoms}=$" + str(N_atoms))


figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

plt.legend()
plt.show()

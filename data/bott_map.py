from params import *

raw = pd.read_csv(path + "0_botts_map.dat")

h_data = pd.pivot(raw, values='CB', index=['deltaB'], columns='deltaAB')

for i in range(1, N_batch):
    try:
        raw = pd.read_csv(path + str(i) + "_botts_map.dat")
        h_data2 = pd.pivot(raw, values='CB', index=['deltaB'], columns='deltaAB')
        h_data += h_data2
    except FileNotFoundError:
        pass

h_data = h_data/N_batch

plt.figure(figsize=(9,7))

ax = sns.heatmap(h_data, cmap="Spectral", cbar=False, vmin=-1, vmax = 1)
ax.set_xlabel("$\Delta_{AB}$")
ax.set_ylabel("$\Delta_{B}$")
ax.locator_params(nbins = 12)
ax.title.set_text('Bott index')

plt.title( "\n $N_{atoms}=$" + str(N_atoms) +
           " | $r/a=$" + str(random) +
           " | $N_{batch}=$" + str(N_batch) +
           " | $\omega=$" + str(omega))

plt.show()

import matplotlib.pyplot as plt
from math import *

a = 1
b = a*sqrt(3)
c = 3/2*a
m = 5 # 3n - 1
d = 1
xstep = a*sqrt(3)
xmax = (m+0.5)*xstep
xmin = (-m+0.5)*xstep
ymax = xmax*tan(pi/6.)+d
ymin = -ymax + a + d - 0.5
x0 = 0.5*xstep
y0max = 2*ymax - a/2
y0min = 2*ymin - a/2

#print(xmax,xmin,ymax,ymin,y0max,y0min)


x = [xmin, x0, xmax, xmax, x0, xmin]
y = [ymin, y0min, ymin, ymax, y0max, ymax]

N = 80
NN = N*N

asub = []
bsub = []




for i in range(-N, N):
    for j in range(-N, N):
        x = (i+1/4*(1+(-1)**(j+1)))*b
        asub.append( (x, j*c) ) 
        bsub.append( (x, j*c + a) )


lattice = asub + bsub

NN = N*N

latx = [lattice[i][0] for i in range(len(lattice))]
laty = [lattice[i][1] for i in range(len(lattice))]


latx_hex = []
laty_hex = []

e = 0.1

for i in range(len(latx)):
    lx = latx[i]
    ly = laty[i]
    a = (ymax - y0max)/(xmin-x0)
    b = y0max - a *x0
    a2 = (y0max - ymax)/(x0-xmax)
    b2 = ymax - a2*xmax
    a3 = (ymin - y0min)/(xmax - x0)
    b3 = y0min - a3*x0
    a4 = (ymin - y0min)/(xmin-x0)
    b4 = y0min - a4*x0
    # if  (ly >= a4*lx + b4 - e) and (ly <= a2*lx + b2 + e) and (ly <= a*lx + b + e) and (ly >= a3*lx + b3 - e):
    #     latx_hex.append(lx)
    #     laty_hex.append(ly)
    if (lx + e >= xmin and lx - e <= xmax and ly - e<= ymax and ly + e>= ymin):
        latx_hex.append(lx)
        laty_hex.append(ly)



plt.plot(latx_hex[:int(len(latx_hex)/2)], laty_hex[:int(len(latx_hex)/2)], linestyle='None', marker='+')
plt.plot(latx_hex[int(len(latx_hex)/2):], laty_hex[int(len(latx_hex)/2):], linestyle='None', marker='+', color='r')
#print(len(latx_hex))
#plt.plot(latx, laty, linestyle='None', marker='+')
plt.show()

f = open("square.dat", "w")
for i in range(len(latx_hex)):
    f.write(str(latx_hex[i]) + "," + str(laty_hex[i]) + "\n")
                
                    
P1 = (xmin, a2*xmin + b2)
P2 = (xmax, a2*xmax + b2)
P3 = (xmin, a4*xmin + b4)
P4 = (xmax, a4*xmax + b4)

def d(P,Q):
    return sqrt( (P[0] - Q[0])**2 + (P[1] - Q[1])**2)

print(xmax-xmin)
print(ymax-ymin)
Px = [P1[0], P2[0], P3[0], P4[0]]
Py = [P1[1], P2[1], P3[1], P4[1]]
plt.plot(Px, Py)
plt.show()

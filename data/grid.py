from params import *

sns.set_style("whitegrid", {'axes.grid' : False})

hexgrid = pd.read_csv("grid.dat")
        
g = sns.relplot(
    data=hexgrid,
    x="x", y="y", hue="I",sizes=(10, 220),
)


N_atoms = 1364

plt.title( "$N_{atoms}=$" + str(N_atoms))
plt.show()
   
        

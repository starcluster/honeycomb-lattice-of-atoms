import matplotlib.pyplot as plt
from math import *

a = 1
b = a*sqrt(3)
c = 3/2*a
m = 8
xstep = a*sqrt(3)
xmax = (m+0.5)*xstep
xmin = (-m+0.5)*xstep
ymax = xmax*tan(pi/6.)
ymin = -ymax + a
x0 = 0.5*xstep
y0max = 2*ymax - a/2
y0min = 2*ymin - a/2

#CERCLE
xcenter = x0
ycenter = 0.5
radius = (xmax - xmin)/2*0.9

def inTheCircle(x,y):
    return (x - xcenter)**2 + (y - ycenter)**2 <= radius**2

print(xmax,xmin,ymax,ymin,y0max,y0min)


x = [xmin, x0, xmax, xmax, x0, xmin]
y = [ymin, y0min, ymin, ymax, y0max, ymax]

N = 30
NN = N*N

asub = []
bsub = []




for i in range(-N, N):
    for j in range(-N, N):
        x = (i+1/4*(1+(-1)**(j+1)))*b
        asub.append( (x, j*c) ) 
        bsub.append( (x, j*c + a) )


lattice = asub + bsub

NN = N*N

latx = [lattice[i][0] for i in range(len(lattice))]
laty = [lattice[i][1] for i in range(len(lattice))]


latx_hex = []
laty_hex = []

for i in range(len(latx)):
    lx = latx[i]
    ly = laty[i]
    if (lx >= xmin) and  (lx <= xmax) and (ly >= (ymin - (-ymin + a/2) * (x-xmax)/(x0-xmax))) and (ly <= ymax*(2-(x-1/2*xstep)/xmax)) and (ly >= (ymin -(-ymin + a/2)*(x-xmin)/(x0-xmin))) and (ly <= ymax*(2 - (-x+1/2*xstep)/xmax)):
        pass
    a = (ymax - y0max)/(xmin-x0)
    b = y0max - a *x0
    a2 = (y0max - ymax)/(x0-xmax)
    b2 = ymax - a2*xmax
    a3 = (ymin - y0min)/(xmax - x0)
    b3 = y0min - a3*x0
    a4 = (ymin - y0min)/(xmin-x0)
    b4 = y0min - a4*x0
    if (lx >= xmin) and  (lx <= xmax) and (ly >= ymin) and (ly <= ymax):
        latx_hex.append(lx)
        laty_hex.append(ly)
    elif (ly >= ymax) and (lx <= x0) and (ly <=  a*lx+b):
        latx_hex.append(lx)
        laty_hex.append(ly)
    elif (ly >= ymax) and (lx >= x0) and (ly <=  a2*lx+b2):
        latx_hex.append(lx)
        laty_hex.append(ly)
    elif (ly <= ymin) and (lx >= x0) and (ly >= a3*lx + b3):
        latx_hex.append(lx)
        laty_hex.append(ly)
    elif (ly <= ymin) and (lx <= x0) and (ly >= a4*lx + b4):
        latx_hex.append(lx)
        laty_hex.append(ly)

latx_b = []
laty_b = []

e = 1

# latx_hex.append(-12)
# laty_hex.append(-8)

for x,y in zip(latx_hex, laty_hex):
    if y > a*x+b - e:
        latx_b.append(x)
        laty_b.append(y)
    elif y > a2*x + b2 - e:
        latx_b.append(x)
        laty_b.append(y)
    elif y < a3*x + b3 + e:
        latx_b.append(x)
        laty_b.append(y)
    elif y < a4*x + b4 + e:
        latx_b.append(x)
        laty_b.append(y)
    elif x >= xmax - e + 1:
        latx_b.append(x)
        laty_b.append(y)
    elif x <= xmin + e - 1:
        latx_b.append(x)
        laty_b.append(y)
    # if not(inTheCircle(x,y)):
    #     latx_b.append(x)
    #     laty_b.append(y)

# latx_hex.append(-12)
# laty_hex.append(-8)
plt.plot(latx_hex[:int(len(latx_hex)/2)], laty_hex[:int(len(latx_hex)/2)], linestyle='None', marker='+')
plt.plot(latx_hex[int(len(latx_hex)/2):], laty_hex[int(len(latx_hex)/2):], linestyle='None', marker='+', color='r')
print(len(latx_hex))
#plt.plot(latx, laty, linestyle='None', marker='+')
plt.show()

plt.plot(latx_b[:int(len(latx_b)/2)], laty_b[:int(len(latx_b)/2)], linestyle='None', marker='+')
plt.plot(latx_b[int(len(latx_b)/2):], laty_b[int(len(latx_b)/2):], linestyle='None', marker='+', color='r')
print(len(latx_b))
#plt.plot(latx, laty, linestyle='None', marker='+')
plt.show()

f = open("hex_m8.dat", "w")
for i in range(len(latx_hex)):
    f.write(str(latx_hex[i]) + "," + str(laty_hex[i]) + "\n")

f2 = open("hex_b.dat", "w")
for i in range(len(latx_b)):
    f2.write(str(latx_b[i]) + "," + str(laty_b[i]) + "\n")
                
                    

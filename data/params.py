import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys

try:
    path = sys.argv[1] + "/"
except IndexError:
    path = "./"

# N, a, deltaB, deltaAB, random, N_batch, omega,N_atoms = map(float, open(path + "params.dat").readlines())
N, a, deltaB, deltaAB, random, N_batch, omega = map(float, open(path + "params.dat").readlines())
# N_atoms = int(N_atoms)
N_batch = int(N_batch)
NN = int(N*N)
N_atoms = 2*NN

L = abs(abs(deltaB)-abs(deltaAB))*2
centre = 6.77
left = centre - L/2
right = centre + L/2

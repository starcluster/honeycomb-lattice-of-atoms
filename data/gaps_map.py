import seaborn as sns
import matplotlib.pyplot as plt
from pandas import Series, DataFrame
import pandas as pd
import numpy as np

sns.set_theme(style="whitegrid")

# Load the example planets dataset
#planets = sns.load_dataset("planets")
gaps_map = pd.read_csv("gaps.dat")
cmap = sns.cubehelix_palette(rot=-0.1, as_cmap=True, start=1)

g = sns.relplot(
    data=gaps_map,
    x="deltaB", y="deltaAB",
     hue="gap", size="gap",
    palette=cmap, sizes=(10, 220),
)

#g.set(xscale="log", yscale="log")
g.ax.xaxis.grid(True, "minor", linewidth=.25)
g.ax.yaxis.grid(True, "minor", linewidth=.25)
g.despine(left=True, bottom=True)

# plt.title("$\omega=$" + str(intensity_hex["f"][0]) +
#           "\n $\Delta_B=$" + str(intensity_hex["deltaB"][0]) +
#           "\n $N_{atoms}=$" + str(intensity_hex["N"][0]) +
#           "\n $\Delta_{AB}=$" + str(intensity_hex["deltaAB"][0]),
#           x=1.1, y=0.05)
plt.show()
plt.savefig("gaps_map.png")
plt.clf()
deltaAB5 = gaps_map.loc[gaps_map['deltaAB'] == 5]
plt.plot(deltaAB5['deltaB'],deltaAB5['gap'], marker='.', linestyle='None')
plt.title(r"$\Delta_{AB} = 5$")
plt.xlabel(r"$\Delta_B$")
plt.ylabel("GAP")
X = np.linspace(-12,12,100)
Y = np.abs(np.abs(X)-5)
plt.plot(X,Y)
plt.show()

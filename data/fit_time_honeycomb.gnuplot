set xlabel "Number of atoms"
set ylabel "Execution time [s]"
set key left top
f(x) = a*x**3 + b*x**2 + c*x # Complexite en O(n^3) sans offset
fit f(x) 'time_exec.dat' u 1:2 via a,b,c
plot f(x), 'time_exec.dat' u 1:2 pt 7 ps 2

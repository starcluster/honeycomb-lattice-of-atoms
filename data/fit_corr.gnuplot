set xlabel "Aléatoire [%]"
set ylabel "Correlation"
A = 1
f(x) = A - B*exp(C*x) 
fit f(x) 'fit_corr.dat' u 1:2 via A,B,C
plot f(x)  t 'A - B*exp(-C*x)', 'fit_corr.dat' t 'Correlations mesurées delta_B = 12, delta_A_B= 0' u 1:2 pt 7 ps 0.4

import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import glob
import os

sns.set_theme(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})

# find the local files
p = 'N10'  # p = Path.cwd()  # for data in the current working directory
# files = list(p.glob('.dat'))

columns = [str(i+1) for i in range(48)]
dfl = []
dic = {'x':[], 'g': []}
lab = 1
for f in os.listdir(p):
    re_eigenvalues = (-1)*np.array(list(map(float, open(p+'/'+f).readlines())))
    # v = pd.read_csv(f, sep='\\s+', header=None, usecols=[1])
    # v.columns = ['intensity']
    # v['label'] = f.suffix.split('_')[-1]
    
    for i in re_eigenvalues:
        dic['x'].append(i)
        dic['g'].append(str(lab))
    lab += 1
    #     try:
    #         dic[str(lab)].append(i)
    #     except KeyError:
    #         dic[str(lab)] = [i]
    # lab += 1
    dfl.append(re_eigenvalues)
print(dic)
#exit()
# Create the data
rs = np.random.RandomState(1979)
x = rs.randn(500)
dfl = np.array(dfl)

dfl2 = pd.DataFrame(dfl.transpose(), columns=columns)

print(dfl2)

g = np.tile(list("ABCDEFGHIJ"), 50)
print(dict(x=x, g=g))
#exit()
df = pd.DataFrame(dic)

print(df)
# Initialize the FacetGrid object
pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
g = sns.FacetGrid(df, row="g", hue="g", aspect=15, height=.5, palette=pal)

# Draw the densities in a few steps
g.map(sns.kdeplot, "x",
      bw_adjust=.1, clip_on=False,
      fill=True, alpha=1, linewidth=1.5)
g.map(sns.kdeplot, "x", clip_on=False, color="w", lw=2, bw_adjust=.1)
g.map(plt.axhline, y=0, lw=2, clip_on=False)


# Define and use a simple function to label the plot in axes coordinates
def label(x, color, label):
    ax = plt.gca()
    ax.text(0, .2, label, fontweight="bold", color=color,
            ha="left", va="center", transform=ax.transAxes)


g.map(label, "x")

# Set the subplots to overlap
g.fig.subplots_adjust(hspace=-.25)

# Remove axes details that don't play well with overlap
g.set_titles("")
g.set(yticks=[])
g.despine(bottom=True, left=True)

plt.show()

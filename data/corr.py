import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

f_params = open("params.dat").readlines()
N = int(f_params[0])
deltaB = float(f_params[2])
deltaAB = float(f_params[3])
random = float(f_params[4])
NN = N*N
N_atoms = 2*NN

N_batch = int(f_params[5])

corr = pd.read_csv('0_corr.dat')
corr2 = pd.read_csv('corrN_batch100deltaAB0/0_corr.dat')

for i in range(1, N_batch):
    try:
        corr['corr'] += pd.read_csv(str(i) + '_corr.dat')['corr']
        corr2['corr'] += pd.read_csv('corrN_batch100deltaAB0/' + str(i) + '_corr.dat')['corr']
    except FileNotFoundError:
        pass

corr['corr'] /= N_batch
corr2['corr'] /= N_batch
# corr2['r'] /= 2 # a enlever, erreurN_batch

corr.to_csv("fit_corr.dat")


plt.plot(corr["r"] ,corr["corr"], label="$\Delta_{AB}=12$,$\Delta_{B}=0$", color='r')
plt.plot(corr2["r"] ,corr2["corr"], label="$\Delta_{AB}=0$,$\Delta_{B}=12$", color='b') 
plt.xlabel("Aléatoire [%]")
plt.ylabel("Correlation")
plt.legend()

plt.title( "\n $N_{atoms}=$" + str(N_atoms) +
           "\n $N_{batch}=$" + str(N_batch))
plt.show()

from params import *

raw_bott = pd.read_csv(path + "0_botts.dat")

h_data_bott = pd.pivot(raw_bott, values='CB', index=['omega'], columns='err')

for i in range(1, N_batch):
    try:
        raw_bott = pd.read_csv(path + str(i) + "_botts.dat")
        h_data2 = pd.pivot(raw_bott, values='CB', index=['omega'], columns='err')
        h_data_bott += h_data2
    except FileNotFoundError:
        pass

h_data_bott = h_data_bott/N_batch
h_data_bott_var = h_data_bott*0

for i in range(0, N_batch):
    try:
        raw_bott = pd.read_csv(path + str(i) + "_botts.dat")
        h_data2 = pd.pivot(raw_bott, values='CB', index=['omega'], columns='err')
        h_data_bott_var += (h_data2-h_data_bott)**2
    except FileNotFoundError:
        pass

h_data_bott_var = h_data_bott_var/N_batch




fig, (ax,ax2) = plt.subplots(ncols=2, figsize=(18,8))
fig.subplots_adjust(wspace=0.1)

sns.heatmap(h_data_bott, ax=ax, cmap="Spectral", cbar=False)#, vmin=-1, vmax=1)
fig.colorbar(ax.collections[0], ax=ax,location="left", use_gridspec=False, pad=0.07)
ax.invert_yaxis()
ax.title.set_text('Bott index')
ax.set_ylabel("Frequency $\omega$")
ax.set_xlabel("Disorder $W=r/a$  [%]")

sns.heatmap(h_data_bott_var, cmap="Spectral", ax=ax2, cbar=False)
fig.colorbar(ax2.collections[0], ax=ax2,location="right", use_gridspec=False, pad=0.05)
ax2.invert_yaxis()
ax2.title.set_text('Variance Bott')
ax2.set_ylabel("Frequency $\omega$ ")
ax2.set_xlabel("Disorder $W=r/a$  [%]")

fig.suptitle( "\n $N_{atoms}=$" + str(N_atoms) +
           " | $\Delta_B=$" + str(deltaB) +
           " | $\Delta_{AB}=$" + str(deltaAB) +
           " | $N_{batch}=$" + str(N_batch))


figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

plt.show()

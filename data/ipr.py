from params import *

raw_ipr = pd.read_csv(path + "0_iprs.dat")

h_data_ipr = pd.pivot(raw_ipr, values='IPR', index=['omega'], columns='err')

plt.plot(raw_ipr['omega'], raw_ipr['IPR'],  marker='o')

plt.title( "\n $N_{atoms}=$" + str(N_atoms) +
           " | $\Delta_B=$" + str(deltaB) +
           " | $\Delta_{AB}=$" + str(deltaAB) +
           " | $N_{batch}=$" + str(N_batch))


figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

plt.show()

from params import *

raw_bott = pd.read_csv(path + "0_dos.dat")
h_data_bott = pd.pivot(raw_bott, values='DOS', index=['omega'], columns='err')

for i in range(1, N_batch):
    try:
        raw_bott = pd.read_csv(path + str(i) + "_dos.dat")
        print(raw_bott)
        h_data2 = pd.pivot(raw_bott, values='DOS', index=['omega'], columns='err')
        h_data_bott += h_data2
    except FileNotFoundError:
        pass

h_data_bott = h_data_bott/N_batch

plt.figure(figsize=(9,7))
ax = sns.heatmap(h_data_bott, cmap="Spectral", cbar=True)# vmin=-1, vmax=0)

ax.invert_yaxis()
ax.locator_params(nbins=10)
ax.set_ylabel("Frequency $\omega$")
ax.set_xlabel("Dirsorder $W=r/a$  [%]")
ax.set_title( "\n $N_{atoms}=$" + str(N_atoms) +
           " | $\Delta_B=$" + str(deltaB) +
           " | $\Delta_{AB}=$" + str(deltaAB) +
           " | $N_{batch}=$" + str(N_batch))
plt.show()

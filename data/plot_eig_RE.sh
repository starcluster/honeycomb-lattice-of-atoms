echo Launching honeycomb.out with N = $1, a = $2, delta_B $3, delta_AB = $4 and ivector = $5
# et=$( echo 0.35*$1*$1*$1 - 6*$1*$1 + 26*$1 | bc)
# echo Estimated time: $et
start=`date +%s.%N`
../build/honeycomb.out $1 $2 $3 $4 $5 > tmp.dat # computing eigenvalues
end=`date +%s.%N`
# sed 's/[(,)]/ /g' tmp.dat > tmp2.dat # parsing 
# awk '{print $1/2}' tmp2.dat > tmp3.dat # get real part and divide by 2
# gnuplot complex.gnuplot -p # plotting in the complex plane
# gnuplot hist.gnuplot -p # plotting histogram
# python3 plot_hist.py
# rm tmp.dat tmp2.dat tmp3.dat # cleaning
# end=`date +%s.%N`
runtime=$( echo "$end - $start" | bc -l )
echo Computing time: $runtime
echo end-start | bc

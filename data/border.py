from params import *

f_border = pd.read_csv("border.dat")
x = f_border["ivector"]
x2 = f_border["f"]
y = f_border["border"]

plt.plot(x2,y, marker='.') #, linestyle='None')
plt.title("Influence of borders\n $N_{atoms} = " + str(N_atoms) +"$ | $\Delta_B = $" + str(deltaB) + " | $\Delta_{AB} = $" + str(deltaAB) + r" | $r/a = $" + str(random))
#plt.xlim(left=-10, right=30)
plt.xlabel(r"$(\omega-\omega_0)/\Gamma_0$")
plt.ylabel("Intensity of borders")
plt.show()

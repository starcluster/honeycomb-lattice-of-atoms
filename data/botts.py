import seaborn as sns
import matplotlib.pyplot as plt
from pandas import Series, DataFrame
import pandas as pd
import numpy as np

sns.set_theme(style="whitegrid")


# Load the example planets dataset
#planets = sns.load_dataset("planets")
CB_map = pd.read_csv("0_botts.dat")
cmap = sns.cubehelix_palette(rot=0.6, as_cmap=True, start=0)

g = sns.relplot(
    data=CB_map,
    x="deltaAB", y="deltaB", hue="CB")

#g.set(xscale="log", yscale="log")
g.ax.xaxis.grid(True, "minor", linewidth=.25)
g.ax.yaxis.grid(True, "minor", linewidth=.25)
g.despine(left=True, bottom=True)

plt.show()

import seaborn as sns
import matplotlib.pyplot as plt
from pandas import Series, DataFrame
import pandas as pd

sns.set_theme(style="whitegrid")

ipr_map = pd.read_csv("iprs.dat")
cmap = sns.cubehelix_palette(rot=-0.9, as_cmap=True, start=1)

f_params = open("params.dat").readlines()
N = int(f_params[0])
deltaB = float(f_params[2])
deltaAB = float(f_params[3])
random = float(f_params[4])
NN = N*N
N_atoms = 2*NN

g = sns.relplot(
    data=ipr_map,
    x="err", y="omega",
    size="IPR", hue="IPR", 
    palette=cmap, sizes=(10, 220),
)

#g.set(xscale="log", yscale="log")
g.ax.xaxis.grid(True, "minor", linewidth=.25)
g.ax.yaxis.grid(True, "minor", linewidth=.25)
g.despine(left=True, bottom=True)

plt.title( "\n $N_{atoms}=$" + str(N_atoms) +
           "\n $\Delta_B=$" + str(deltaB) +
           "\n $\Delta_{AB}=$" + str(deltaAB),
                     x=1.1, y=0.05)
plt.show()
plt.savefig("intensity_map.png")

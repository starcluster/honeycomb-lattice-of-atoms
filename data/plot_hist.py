from params import *

f_freqs = pd.read_csv("freq.dat")
re_eigenvalues = f_freqs["f"]

#sns.set_style('darkgrid')


sns.displot(re_eigenvalues, binwidth=2, element="step", stat="density", height=8.27, aspect=12/8.27)
sns.kdeplot(re_eigenvalues, bw_adjust=0.1, color="r")
#plt.title("$N_{atoms} = " + str(N_atoms) +"$ | $\Delta_B = $" + str(deltaB) + " | $\Delta_{AB} = $" + str(deltaAB) + r" | $r/a = $" + str(random))
# N_atoms = 726
#plt.axhline(y=1/(2*N_atoms), color='g', linestyle='--', label="$1/2N_{atoms}$")
plt.axvline(x=left, color='g', linestyle='--', ymax = 0.5)#, label="$1/2N_{atoms}$")
plt.axvline(x=right, color='g', linestyle='--', ymax = 0.5)#, label="$1/2N_{atoms}$")
#plt.axhline(y=1/(N_atoms), color='b', linestyle='--')
# plt.axhline(y=1/14/14/2, color='g', linestyle='--')
plt.xlabel(r"$(\omega-\omega_0)/\Gamma_0$")
plt.ylabel("DOS")

title = "$N_{atoms} = " + str(N_atoms) +"$ | $\Delta_B = $" + str(deltaB) + " | $\Delta_{AB} = $" + str(deltaAB) + r" | $r/a = $" + str(random)
plt.figtext(.5,.8,title, fontsize=20, ha='center')


#plt.legend()
plt.show()

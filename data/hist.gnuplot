set xlabel "Frequency w-w0/Gamma0"
set ylabel "DOS"
binwidth=1
set boxwidth binwidth
bin(x, w) = w*floor(x/w)
set arrow from -17,0 to -17,50 nohead lc rgb 'red' dt 2
set arrow from 7,0 to 7,50 nohead lc rgb 'red' dt 2
plot 'tmp3.dat' u (bin($1, binwidth)):(1.0) smooth freq with boxes lc rgb "blue" title "N atoms=: histogram of eigenvalues"

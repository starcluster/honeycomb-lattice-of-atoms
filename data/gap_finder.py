import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

f1 = pd.read_csv("freq8.dat")
f2 = pd.read_csv("freq10.dat")


sns.set_style('darkgrid')
# sns.displot(f1["f"], binwidth=2, element="step", stat="density")
# sns.displot(f2["f"], binwidth=2, element="step", stat="density")

sns.kdeplot(f1["f"], bw_adjust=0.12, color="r")
sns.kdeplot(f2["f"], bw_adjust=0.12, color="b")

# plt.axhline(y=1/8/8/8, color='b', linestyle='--')

plt.xlabel(r"$(\omega-\omega_0)/\Gamma_0$")
plt.ylabel("DOS")
#plt.title(r"Histogram $\Delta_{AB}=12$, $N=16$") 
plt.show()

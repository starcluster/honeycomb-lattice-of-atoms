import seaborn as sns
import matplotlib.pyplot as plt
from pandas import Series, DataFrame
import pandas as pd

sns.set_theme(style="whitegrid")

# Load the example planets dataset
#planets = sns.load_dataset("planets")
for i in range(1,399):
    intensity_hex = pd.read_csv("heatmaps/" + str(i) + ".dat")
    cmap = sns.cubehelix_palette(rot=-0.9, as_cmap=True, start=1)

    g = sns.relplot(
        data=intensity_hex,
        x="x", y="y",
        size="I", hue="I", 
        palette=cmap, sizes=(10, 220),
    )
    #g.set(xscale="log", yscale="log")
    g.ax.xaxis.grid(True, "minor", linewidth=.25)
    g.ax.yaxis.grid(True, "minor", linewidth=.25)
    g.despine(left=True, bottom=True)
    
    plt.title("$\omega=$" + str(intensity_hex["f"][0]), x=1.1, y=0.05)
    plt.savefig("heatmaps_fig/" + str(i) + ".png")

import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

f_gaps = pd.read_csv("gap_disorder.dat")
gaps = f_gaps["gap"]
f_params = open("params.dat").readlines()
N = int(f_params[0])
deltaB = float(f_params[2])
deltaAB = float(f_params[3])
NN = N*N
N_atoms = 2*NN


plt.plot(gaps)
plt.show()

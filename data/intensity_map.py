from params import *

sns.set_style("whitegrid", {'axes.grid' : False})

#cmap = sns.cubehelix_palette(rot=3, as_cmap=True, start=-1)
cmap = sns.cubehelix_palette(as_cmap=True, start=2, rot=-1, gamma=2)
#sns.color_palette("husl", 8)

for i in range(0,8*NN):
    try:
        print(i)
        intensity_hex = pd.read_csv(str(i) + "_intensity_map.dat")
        
        g = sns.relplot(
            data=intensity_hex,
            x="x", y="y",
            size="I", hue="I", 
            sizes=(10, 220),
        )

        omega = intensity_hex["f"][0]

        plt.title( "$N_{atoms}=$" + str(N_atoms) +
                   " | $\omega=$" + str(omega) +
                   " | $\Delta_B=$" + str(deltaB) +
                   " | $\Delta_{AB}=$" + str(deltaAB) +
                   " | $r/a=$" + str(random))
        plt.show()
    except FileNotFoundError:
        pass
        

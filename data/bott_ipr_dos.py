from params import *

raw_bott = pd.read_csv(path + "0_botts.dat")
raw_ipr = pd.read_csv(path + "0_iprs.dat")
raw_dos = pd.read_csv(path + "0_dos.dat")

h_data_bott = pd.pivot(raw_bott, values='CB', index=['omega'], columns='err')
h_data_ipr = pd.pivot(raw_ipr, values='IPR', index=['omega'], columns='err')
h_data_dos = pd.pivot(raw_dos, values='DOS', index=['omega'], columns='err')

for i in range(1, N_batch):
    try:
        raw_bott = pd.read_csv(path + str(i) + "_botts.dat")
        raw_ipr = pd.read_csv(path + str(i) + "_iprs.dat")
        raw_dos = pd.read_csv(path + str(i) + "_dos.dat")
        h_data2 = pd.pivot(raw_bott, values='CB', index=['omega'], columns='err')
        h_data4 = pd.pivot(raw_ipr, values='IPR', index=['omega'], columns='err')
        h_data = pd.pivot(raw_dos, values='DOS', index=['omega'], columns='err')
        h_data_bott += h_data2
        h_data_ipr += h_data4
        h_data_dos += h_data
    except FileNotFoundError:
        pass

h_data_bott = h_data_bott/N_batch
h_data_ipr = h_data_ipr/N_batch
h_data_dos = h_data_dos/N_batch
b = int(raw_bott["omega"][0])
b = -10
print(b)

fig, (ax,ax2,ax3) = plt.subplots(ncols=3, figsize=(18,8))
fig.subplots_adjust(wspace=0.1)

sns.heatmap(h_data_bott, ax=ax, cmap="Spectral", cbar=False)#, vmin=-1, vmax=1)
fig.colorbar(ax.collections[0], ax=ax,location="top", use_gridspec=False, pad=0)
fig.axes[0].axhline(y=right-b, color='w', zorder=10)
fig.axes[0].axhline(y=left-b, color='w', zorder=10)
ax.invert_yaxis()
ax.title.set_text('Bott index')
ax.set_ylabel("Fréquence $\omega$")
ax.set_xlabel("Désordre $W=r/a$  [%]")

sns.heatmap(h_data_ipr, cmap="Spectral", ax=ax2, cbar=False)
fig.colorbar(ax2.collections[0], ax=ax2,location="top", use_gridspec=False, pad=0.0)
fig.axes[1].axhline(y=right-b, color='w', zorder=10)
fig.axes[1].axhline(y=left-b, color='w', zorder=10)
ax2.invert_yaxis()
ax2.title.set_text('IPR')
ax2.set_ylabel("Fréquence $\omega$")
ax2.set_xlabel("Désordre $W=r/a$  [%]")

sns.heatmap(h_data_dos, cmap="Spectral", ax=ax3, cbar=False)
fig.colorbar(ax3.collections[0], ax=ax3,location="top", use_gridspec=False, pad =0)
fig.axes[2].axhline(y=right-b, color='w', zorder=10)
fig.axes[2].axhline(y=left-b, color='w', zorder=10)
ax3.invert_yaxis()
ax3.title.set_text('DOS')
ax3.set_ylabel("Fréquence $\omega$")
ax3.set_xlabel("Désordre $W=r/a$  [%]")

fig.suptitle( "\n $N_{atoms}=$" + str(N_atoms) +
           " | $\Delta_B=$" + str(deltaB) +
           " | $\Delta_{AB}=$" + str(deltaAB) +
              " | $N_{batch}=$" + str(N_batch), fontsize='xx-large')


figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()
#fig.subplots_adjust(left=0, right=1, top=0.7, bottom=0)
plt.savefig('B' + str(int(deltaB)) + 'AB' + str(int(deltaAB)) + 'N20_B.png',bbox_inches='tight')
fig.tight_layout()
plt.show()

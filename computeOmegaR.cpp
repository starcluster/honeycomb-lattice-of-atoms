#include "computeOmegaR.h"


using namespace std;

void computeOmegaR(Params p, int i_batch){
  int NN = p.N*p.N;
  int start = (int) p.r_inf*(1./p.r_step);
  int end = (int) p.r_sup*(1./p.r_step);
  int start2 = (int) p.omega_inf*(1./p.omega_step);
  int end2 = (int) p.omega_sup*(1./p.omega_step);
  double t0 = omp_get_wtime();
  double t0_cpu = clock();

  ofstream f_ipr_init(p.output + to_string(i_batch) + "_iprs.dat");
  f_ipr_init << "omega,err,IPR"<<endl;
  f_ipr_init.close();

  ofstream f_bott_init(p.output + to_string(i_batch) + "_botts.dat");
  f_bott_init << "CB,omega,err"<<endl;
  f_bott_init.close();

  ofstream f_dos_init(p.output + to_string(i_batch) + "_dos.dat");
  f_dos_init << "DOS,omega,err"<<endl;
  f_dos_init.close();
  
  //#pragma omp parallel for
  for(int r = start; r <= end; r++){
          cout << "omega " << r <<endl;
	  //p.N_atoms = 294;
    double freqs[2*p.N_atoms], gammas[2*p.N_atoms];
    MatrixXcd Q(2*p.N_atoms, 2*p.N_atoms);
    MatrixXcd UX(p.N_atoms, p.N_atoms), UY(p.N_atoms, p.N_atoms);
    MatrixXcd VX(2*p.N_atoms, 2*p.N_atoms), VY(2*p.N_atoms, 2*p.N_atoms);
    UX = MatrixXcd::Zero(p.N_atoms, p.N_atoms);
    UY = MatrixXcd::Zero(p.N_atoms, p.N_atoms);
    VX = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);
    VY = MatrixXcd::Zero(2*p.N_atoms, 2*p.N_atoms);
    p.random = r*p.r_step;
    Q = generalProcedure(p, freqs, gammas, &UX, &UY, &VX, &VY, r*p.r_step);
    int ivector_pos = 0;
    #pragma omp parallel for
    for(int omega = start2; omega <= end2; omega++){

      int ivector = 0;
      //IPR
      double omega_practical = omegaPractical(freqs, omega*p.omega_step, &ivector, p.N_atoms);
      cout << "omega p " << omega_practical << endl;
      //cout << ivector << endl;
      //double abc;
      //      cin >> abc;
      double ipr_m = ipr(Q, p.N_atoms, ivector);
      cout << "blabla" <<endl;
      ofstream f_ipr(p.output + to_string(i_batch) + "_iprs.dat", ios_base::app);
      // if (ivector == ivector_pos){;}else {
	if (f_ipr) f_ipr << omega*p.omega_step << "," << r*p.r_step << "," << ipr_m <<endl; else fileError();
      f_ipr.close();
      ivector_pos = ivector;
      //}
      //BOTT
      ofstream f_bott(p.output + to_string(i_batch) + "_botts.dat", ios_base::app);
      if (f_bott){
	f_bott << bott(freqs, omega_practical,&VX, &VY) << "," << omega*p.omega_step << "," << r*p.r_step <<endl;
      } else {fileError();}
      f_bott.close();
      //DOS
      double dos = DOS(freqs, omega-p.omega_step/2., omega+p.omega_step/2., p.N_atoms);
      ofstream f_dos(p.output + to_string(i_batch) + "_dos.dat", ios_base::app);
      if (f_dos){
	f_dos << dos << "," << omega*p.omega_step << "," << r*p.r_step <<endl;
      } else {fileError();}
      f_dos.close();
    }
  }

  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  cout << "Temps reel (compute omega/R): " << t1-t0 << endl;
  cout << "Temps CPU: (compute omega/R)" << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;
}

double DOS(double *freqs, double omega1, double omega2, int N_atoms){
  int j = 0;
  for(int i = 0; i < 2*N_atoms; ++i){
    if (freqs[i] > omega1 and freqs[i] < omega2){
      j++;
    }
  }
  return ((double)j)/((double)2*N_atoms); //((double)j)/((double)4*NN);
}


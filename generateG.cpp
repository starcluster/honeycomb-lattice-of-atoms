#include "generateG.h"
#include <ctime>
#include <cstdlib>
#include <vector>

void generateG2D(Params *p, Vector2d *hexlattice, MatrixXcd *G2){
  const int NN = p->N*p->N;
  const double k_0 = 2*M_PI*0.05/p->a;
  const int N_atoms = p->N_atoms; // 2 sublattices of NN atoms

  int sgn;
  double r, dx, dy, sr1_2 = 1.0/sqrt(2), deltaR, deltaPhi, deltaX, deltaY;
  complex<double> tmp1, tmp2, tmp3;
  MatrixXcd G(2*p->N_atoms, 2*p->N_atoms);
  Matrix2cd deg, block;

  deg << 1./sqrt(2), 1.0id*sr1_2, -1./sqrt(2), 1.0id*sr1_2;

  // --> generate the hex network: sublattices and total lattice
  generateHex(hexlattice, p->N, p->a);
  
  srand(time(nullptr));

  if (p->A_only){
    for(int i = 0; i < NN; i++){ // FACTORIZE !!
      deltaR = rand()/100/(RAND_MAX + 0.01)*p->random;
      deltaPhi = rand()/(RAND_MAX + 0.01)*2*M_PI;
      
      deltaX = deltaR*cos(deltaPhi);
      deltaY = deltaR*sin(deltaPhi);
      hexlattice[i](0) += deltaX;
      hexlattice[i](1) += deltaY;
    }
  } else if (p->B_only) {
        for(int i = NN; i < 2*NN; i++){ 
	  deltaR = rand()/100/(RAND_MAX + 0.01)*p->random;
	  deltaPhi = rand()/(RAND_MAX + 0.01)*2*M_PI;
	  
	  deltaX = deltaR*cos(deltaPhi);
	  deltaY = deltaR*sin(deltaPhi);
	  hexlattice[i](0) += deltaX;
	  hexlattice[i](1) += deltaY;
	}
  } else {
    for(int i = 0; i < 2*NN; i++){ 
      deltaR = rand()/100/(RAND_MAX + 0.01)*p->random;
      deltaPhi = rand()/(RAND_MAX + 0.01)*2*M_PI;
      
      deltaX = deltaR*cos(deltaPhi);
      deltaY = deltaR*sin(deltaPhi);
      hexlattice[i](0) += deltaX;
      hexlattice[i](1) += deltaY;
    }
  }




  
  for(int i = 0; i < N_atoms; i++){
    for(int j = 0; j < N_atoms; j++){ // we fill the matrix by 2x2 blocks
      if (i==j){
	Matrix2cd O;
	O << 0,0,0,0;
	G.block(i,j,2,2) =  O; // ??
      } else {
	dx = k_0*p->a*(hexlattice[i](0) - hexlattice[j](0));
	dy = k_0*p->a*(hexlattice[i](1) - hexlattice[j](1));
	r = sqrt(dx*dx + dy*dy);
	tmp1 = 3./2.*cexp(1.0id*r)/r;
	tmp2 = P(1.0id*r);
	tmp3 = Q(1.0id*r);
	G(2*i, 2*j) = tmp1*(tmp2+tmp3*dx*dx/(r*r));
	G(2*i, 2*j + 1) = tmp1*(tmp3*dx*dy/(r*r));
	G(2*i + 1, 2*j) = tmp1*(tmp3*dx*dy/(r*r));
	G(2*i + 1, 2*j + 1) = tmp1*(tmp2+tmp3*dy*dy/(r*r));
      }
    }
  }
  
  for(int i = 0; i < N_atoms; i++){
    for(int j = 0; j < N_atoms; j++){ // we fill the matrix by 2x2 blocks
      if (i==j){
	if (i < N_atoms/2) { sgn = 1; } else {sgn = -1;}
	G(2*i, 2*j) = 1.0id + 2*(p->delta_B) + 2*sgn*(p->delta_AB);
	G(2*i + 1, 2*j + 1) = 1.0id - 2*(p->delta_B) + 2*sgn*(p->delta_AB);
	G(2*i+1, 2*j) = 0;
	G(2*i, 2*j+1) = 0;
      } else {
	block = G.block(2*i,2*j,2,2);
	G.block(2*i,2*j,2,2) = deg*block*deg.adjoint();
      }
    }
  }

  for(int i = 0; i < 2*p->N_atoms; i++){
    for(int j = 0; j < 2*p->N_atoms; j++){
      //      cout << G(i,j) << " " << i << " " << j << endl;
      (*G2)(i,j) = G(i,j);
    }
  }
}

  

complex<double> P(complex<double> x){
  return 1. - 1./x + 1./pow(x,2);
}

complex<double> Q(complex<double> x){
  return -1. + 3./x - 3./pow(x,2);
}

void generateHex(Vector2d * hexlattice, int N, double a){
  int NN = N*N;
  const double b = a*sqrt(3);
  const double c = 3./2.*a;

  Vector2d *asublattice, *bsublattice;
  asublattice = new Vector2d[NN];
  bsublattice = new Vector2d[NN];

  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      asublattice[i+j*N] = Vector2d((i+1./4.*(1+pow(-1, j+1)))*b, j*c);
      bsublattice[i+j*N] = Vector2d((i+1./4.*(1+pow(-1, j+1)))*b, j*c + a);
    }
  }
  for(int i = 0; i < N*N; i++){
    hexlattice[i] = asublattice[i];
    hexlattice[i + N*N] = bsublattice[i];
  }
}





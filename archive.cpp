#include "honeycomb.h"

// This file is kept as a potential ressource, it is completely
// useless in the final version.


void generateHexGrid(Vector2d * HexGrid, int N){
  Vector2d a1(0, 1);
  for(int i = 0; i < N; i++){
    HexGrid[i] = a1;
  }
}
  
MatrixXcd directProduct2D(Vector2d a, Vector2d b){
  double xa = a[0], ya = a[1], xb = b[0], yb = b[1];
  MatrixXcd dp(2,2);
  dp << xa*xa, xa*yb, ya*xb, yb*yb;
  return dp;
 }

MatrixXcd directProduct3D(Vector2d a, Vector2d b){
  double xa = a[0], ya = a[1], xb = b[0], yb = b[1];
  MatrixXcd dp(3,3);
  dp << xa*xa, xa*yb, 0,  ya*xb, yb*yb, 0, 0, 0, 1;
  return dp;
 }
   
MatrixXcd generateG3D(int N, double a, double delta_AB, double delta_B){
  const int NN = N*N;
  const double k_0 =  2*M_PI*0.05/a;
  double r = 0;
  
  MatrixXcd I3 = Matrix<double, 3, 3>::Identity();
  Matrix3cd LHS, RHS;
  
  double sr1_2 = 1.0/sqrt(2);
  MatrixXcd deg(3,3);
  deg << 1./sqrt(2), 1.0id*sr1_2, 0.0,-1./sqrt(2), 1.0id*sr1_2, 0.0, 0.0, 0.0, 1.0;
		      
  MatrixXcd G(3*NN, 3*NN);

  HexGrid h(N, a);
  
  for(int i = 0; i < NN; i++){
    for(int j = 0; j < NN; j++){
      int m = 3*i;
      int n = 3*j; // we look only for 3x3 blocks
      if ((m+n)%2==0){
	LHS = (1.0id + 2*delta_AB - 2*delta_B)*(1.*delta(m,n))*I3;
	
      } else {
	LHS = (1.0id - 2*delta_AB - 2*delta_B)*(1.*delta(m,n))*I3;
      }
      Vector2d r_mn = h.get(i) - h.get(j);
      r = r_mn.norm();
      if (r!=0.){
	RHS = (1.-delta(m,n))*cexp(1.id*k_0*r)/k_0/r*I3;
      } else { RHS = 0*RHS;//RHS = O
      }
      Matrix3cd SUM;
      Matrix3cd dp = directProduct3D(r_mn, r_mn);
      if (r!=0)
	SUM = P(1.id*k_0*r)*I3 + Q(1.id*k_0*r)*I3*dp/pow(r,2);
      RHS = RHS*SUM;
      G.block(m,n,3,3) =  LHS + deg*RHS*deg.adjoint();
    }
  }
  return G;
}

MatrixXcd generateG2D(int N, double a, double lambda, double delta_AB, double delta_B){
  const int NN = N*N;
  const double k_0 =  2*M_PI*0.05/a;
  double r = 0;
  
  MatrixXcd I2 = Matrix<double, 2, 2>::Identity();
  Matrix2cd LHS, RHS;
  
  double sr1_2 = 1.0/sqrt(2);
  MatrixXcd deg(2,2);
  deg << 1./sqrt(2), 1.0id*sr1_2, -1./sqrt(2), 1.0id*sr1_2;
		      
  MatrixXcd G(2*NN, 2*NN);

  HexGrid h(N, a);
  for(int i = 0; i < NN; i++){
    for(int j = 0; j < NN; j++){
      int m = 2*i;
      int n = 2*j; // we look only for 2x2 blocks
      if (i%2==0){
	LHS << (1.0id + 2*delta_AB + 2*delta_B)*(1.*delta(m,n)), 0, 0, (1.0id + 2*delta_AB - 2*delta_B)*(1.*delta(m,n));
	
      } else {
	LHS << (1.0id - 2*delta_AB + 2*delta_B)*(1.*delta(m,n)), 0, 0, (1.0id - 2*delta_AB - 2*delta_B)*(1.*delta(m,n));
      }
      Vector2d r_mn = h.get(i) - h.get(j);
      r = r_mn.norm();
      if (r!=0.){
	RHS = (1.-delta(m,n))*cexp(1.id*k_0*r)/k_0/r*I2;
      } else { RHS = 0*RHS;//RHS = O
      }
      Matrix2cd SUM;
      Matrix2cd dp = directProduct2D(r_mn, r_mn);
      if (r!=0)
	SUM = P(1.id*k_0*r)*I2 + Q(1.id*k_0*r)*I2*dp/pow(r,2);
      RHS = RHS*SUM;
      G.block(m,n,2,2) =  LHS + deg*RHS*deg.adjoint();
    }
  }
  return G;
}


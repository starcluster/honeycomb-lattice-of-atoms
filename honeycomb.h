#include <iostream>
#include <fstream>
#include <complex>
#include <Eigen/Dense>
#include <sstream>
#include <string>

#include "generateG.h"
#include "modes.h"
#include "bott.h"
#include "intensityMap.h"
#include "utils.h"
#include "ipr.h"
#include "omp.h"
#include "computeOmegaR.h"

using namespace std;
using namespace Eigen;





#include "HexGrid.h"

using namespace Eigen;
using namespace std;

HexGrid::HexGrid(int size_of_grid, double size_of_step){
  N = size_of_grid;
  if (N%2!=0){
    cout << "The size of the grid must be an even number ! ABORT" <<endl;
    abort();
  }
  a = size_of_step;
  a1 << 0, a;
  a2 <<  -a*sqrt(3)/2, -a/2;
  a3 << a*sqrt(3)/2, -a/2;   // basis vectors

  vertical = a1 - a2 -a3;
  Grid = new Vector2d[N*N];
  Vector2d vect_i(0,0);
  for(int i = 0; i < N; i+=2){
    Grid[i*N] = vect_i;
    vect_i += vertical;
    Grid[(i+1)*N] = vect_i;
    vect_i += a1;
  }
  for(int i = 0; i < N; i++){
    vect_i = Grid[i*N];
      if (i%2==0){
	for(int j = 0; j < N-2; j+=2){
	vect_i += -a2;
	Grid[i*N + j+1] = vect_i;
	vect_i += a3;
	Grid[i*N + j+2] = vect_i;
	}
	Grid[i*N + N-1] = vect_i - a2;
      }
      else{
	for(int j = 0; j < N-2; j+=2){
	  vect_i += a3;
	  Grid[i*N + j+1] = vect_i;
	  vect_i += -a2;
	  Grid[i*N + j+2] = vect_i;
	}
	Grid[i*N + N-1] = vect_i + a3;
      }
  }
}

Vector2d HexGrid::get(int i){
  if (i<0 or i>N*N){
    cout << "Atom is inexistent ! ABORT" <<endl;
    abort();
  }
  return Grid[i];
}

Vector2d HexGrid::get(int i, int j){
  return Grid[i*N + j];
}

void HexGrid::print(){
  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      cout << get(i,j).transpose() << endl;
    }
  }
}

void HexGrid::print_A(){
  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      if ((i+j)%2==0)
	cout << get(i,j).transpose() << endl;
    }
  }
}

void HexGrid::print_B(){
  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      if ((i+j)%2==1)
	cout << get(i,j).transpose() << endl;
    }
  }
}


HexGrid::~HexGrid(){
  delete[] Grid;
}
